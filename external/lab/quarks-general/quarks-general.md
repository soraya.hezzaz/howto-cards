---
layout: page
permalink: /external/lab/quarks-general/
shortcut: lab:quarks-general
redirect_from:
  - /cards/lab:quarks-general
  - /external/cards/lab:quarks-general
---

# Quarks - General information

## How to access Quarks?

https://quarks.lcsb.uni.lu

### What does mean SSO connection?

SSO connection means connection with Uni credentials. name.lastname@uni.lu and associated password

(VPN connection is required if not connected to the local network)
Use the Quarks website with the computer to enter the new batch number and final location of the product
Use the Quarks application to enter the batch number and final location of the chemical products received (by scanning)
## How is organized the Quarks flow at LCSB?
1. Ordering an article via the purchase platform in Quarks

2. If it is a chemical, the product is automatically created in the chemical product inventory of Quarks. Biological reagents and medication are also automatically created in the chemical product inventory.
3. Reception of the article by support team. If it is a chemical, the label is printed and placed in the dedicated box.
4. The buyer come to pick up the deliveries and the labels
5. The buyer

     - Use the Quarks website with the computer to enter the new batch number and final location of the product

     - Use the Quarks application to enter the batch number and final location of the chemical products received (by scanning)


## How are organized the Quarks devices?

- Computers of the lab or personal computers allow to access the website

- Scanners also called the TC20 allow to use the Quarks application in order to move, discard or get information about a product by scanning its Quarks label. To connect to the application, you have to use your uni credentials.

<div align="center">
<img src="img/40.png">
</div>

- In the laboratories, you can also acces the website or the application using the tablets.

- Scanners and tablets have charging support to ensure it is always charged.

<div align="center">
<img src="img/41.png">
<img src="img/42.png">
</div>


- The printers available in the laboratories are linked via USB to the adjacent computer. Those printers are also linked to the network of the University.

## How to print labels?
**From your personal compute**

Please find here the instruction to install the printer for windows users:
https://www.zebra.com/us/en/support-downloads/printer-software/printer-setup-utilities.html

Zebra Printer Setup Utility Support &  Downloads | Zebra
www.zebra.com

Download Zebra's Printer Setup Utility, an easy way to quickly and easily configure select Zebra industrial, mobile and desktop printers.
Download the app : Zebra Setup Utilities for Windows (1-Oct-2019)

 Once it is done, you need to install the printer:

1. Install New Printer
2. Next
3. Install Printer
4. Look for the printer ZDesigner GK420t
5. Next
6. Add Port
7. Next
8. IP Address : 10.213.18.13
9. Next
10. Select the 2 sub-programs Launch
11. Finish


2 softwares will be installed on your computer. The first one is for the printer, the second one is for the bar codes.

IP addresses:
- BT2-Support 10.213.17.166
- BT2-1   10.213.17.167
- BT2-2   10.213.17.207
- BT2-3   10.213.18.13
- BT1-Support
- BT1-4   10.213.19.164
- BT1-5   10.213.19.138

When you will print the label in Quarks, choose the **ZebraGK420t printer** and the following parameters.

In **preferences** you will be able to choose:
<div align="center">
<img src=" img/43.png">
 </div>
And
<div align="center">
<img src=" img/44.png">
</div>
Go in **printers and scanners** and right click on your printer (zebra). You will have printer preferences and should fix it for your session. Click on **apply**.

**From the laboratory – USB link**

The printers are in the open laboratories, generally in the center of the open lab. Please if you canno’t find it, check with your technicians. There is one printer for each floor. We have additional printer on first floor BT1 and second floor BT2 for support team. Support team is providing the labels.


## In which cases will I need to print labels?

You have to print a label for chemicals, kits, biological reagents, homemade solutions, reconditioning labels

## What are the roles in Quarks?

Quarks gives us the possibility to have different roles profiles, with different rights.

**Viewer**: only view orders from own group

**Researcher**: place article in a cart and view orders from/for own group, create new article

**Technician**: place article in a cart and view orders from/for own group, create new articles, receiving notifications on status of all orders

**Budget responsible**: validate carts of own group, choose budget codes for orders, have an overview of the spending

## How is a newcomer assigned to a group?

After first log in in Quarks, the Support team will attribute a role and a group to the newcomer.

# Help needed?
In case of any question, request or issue, [please send a ticket](https://service.uni.lu/sp?id=bs_dep_catalog_entry&sys_id=0840cf71dbaa14148bcbf9b41d9619cb).

