---
layout: page
permalink: /external/lab/maintenance_of_cold_traps/
shortcut: lab:maintenance_of_cold_traps
redirect_from:
  - /cards/lab:maintenance_of_cold_traps
  - /external/cards/lab:maintenance_of_cold_traps
---
# Maintenance of cold traps  

## Labconco Centrivap concentrator and cold trap

The procedure describes the steps for routine maintenance of the Labconco Centrivap concentrators and cold traps.
This maintenance is important to preserve the integrity of the glass bottles inside the cold trap.  

**The Instrument care officer** is responsible for cleaning the condenser once a year, changing the acid filter when necessary and filling the cold trap maintenance record sheet.  

**The Users** are responsible for the routine maintenance of the instrument and filling the “cold trap maintenance record sheet”.  

## Description  

To ensure proper functioning of the centrivap concentrators and associated cold trap, a routine maintenance is necessary:  

<div align=center>

**_Personal Protective Equipment_**:  
Lab coat  
Nitrile gloves

</div>

1.	This routine maintenance is performed by the users of the instrument.  

2.	If a glass bottle is used inside the cold trap, add 99% ethanol to the stainless steel trap until the glass bottle is at least two-thirds immersed.  

3.	At the end of each week, the cold trap should be switched off and the condensed solvent in the glass bottle should be discarded in the corresponding waste container.  

4.	If there is any ice in the stainless steel trap (outside the glass bottle), let it melt, drain the ethanol and replace it with fresh ethanol.
To drain, use the faucet on the left side of the cold trap.  

<div align="center">
<img src="img/cold-trap_img_1.png" height="250">
</div>  

5.	Fill the “cold trap maintenance record sheet “ (Appendix 01)  

6.	Restart the cold trap at the beginning of the following week

**Once a year, the refrigeration system condenser of the Cold trap should be cleaned.**  

<div align=center>

**_Personal Protective Equipment_**:  
Lab coat  
Nitrile gloves  
Safety goggles  

</div> 

1.	This step is performed by a maintenance officer  

2.	Switch off the instrument  

3.	Use a vacuum cleaner with brush attachment  

4.	Clean the condenser (on the left side of the cold trap) to ensure proper airflow for peak performance

<div align="center">
<img src="img/cold-trap_img_2.png" height="250">
</div>  

5.	Fill the “cold trap maintenance record sheet “ (Appendix 01)
When necessary, the acid cartridge (LIMS ID: 10608) should be exchanged  

* When the cartridge looks wet (see arrow on the following picture), it should be exchanged for a new one.  

<div align="center">
<img src="img/cold-trap_img_3.png" height="250">
</div>  

* This is done by a maintenance officer  

* Switch off the instrument  

* Unscrew the black cap out of the transparent container  

* Remove the cartridge and replace it with the new one  

* Close back the container  

* Place the old cartridge in the chemical hood for evaporation in a retention container  

* 24 hours later, trash it in a blue bin UN3291  
