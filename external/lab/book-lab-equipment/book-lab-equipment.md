---
layout: page
permalink: /external/lab/book-lab-equipment/
shortcut: lab:book-lab-equipment
redirect_from:
  - /cards/lab:book-lab-equipment
  - /external/cards/lab:book-lab-equipment
---

# How to book a Lab Equipment in Quarks

To help the lab users to plan their work and to ensure that a lab equipment will be free to use, the Quarks software provides a booking tool. This booking tool is also used by the support team to indicate the maintenance periods, or if an equipment is not available due to a failure or a breakdown.

 - [How to look for an instrument](#how-to-look-for-an-instrument)
 - [How to know if an instrument is bookable](#how-to-know-if-an-instrument-is-bookable)
 - [How to add an equipment to your Favorites list](#how-to-add-an-equipment-to-your-favorites-list)
 - [How to book an instrument](#how-to-book-an-instrument)
 - [How to edit or cancel a booking](#how-to-edit-or-cancel-a-booking)
 - [How to book a lab (location)](#how-to-book-a-lab-(location))
 - [How to access the booking history](#how-to-access-the-booking-history)
 - [How to request booking configuration change or report an issue with Quarks?](#how-to-request-booking-configuration-change-or-report-an-issue-with-Quarks?)

## How to look for an instrument

In your Quarks account, go on the _Equipment_ platform. In the _Equipment_ section, you have a search tool to help you find an equipment among all the LCSB equipment. You can search by internal reference number (e.g. LCSB01685), model, manufacturer or serial number in the “quick search” field.
If you want to add more search criteria, you can do so in the drop down list “Add a filter”.

![1](img/1.png)

## How to know if an instrument is bookable

You can easily know if the instrument you are looking for is bookable or not thanks to the calendar symbol in the “Actions” column.

![2](img/2.png)

You can also see if the instrument is bookable or not on the side panel that will appear by clicking on the instrument itself, in the reservation section. Instrument that might be used for long experiment, possibly overnight, have the “24h bookable” option.

![3](img/3.png)

A bookable instrument will have a calendar icon available. Typically, only specific equipment or equipment that is usually used for long experiments is bookable.

## How to add an equipment to your Favorites list

In the “Actions” tools, you have the option to add an instrument in your favorite list by clicking on the heart.

![4](img/4.png)

To have a direct access to your favorite equipment, go in “Equipment” – “My favorites”.

![5](img/5.png)

For your favorite equipment, you have the option to receive notifications by email. You will receive a notification if someone has edited or cancelled a booking just before yours, like in the example below. You can decide to activate this function for equipment heavily booked.

![6](img/6.png)

To receive the notifications, click on the edit icon and select Yes for the Notifications.

![7](img/7.png)

![8](img/8.png)

You can directly see for which instrument you have the notifications activated by looking in the Notifications column.

![9](img/9.png)

## How to book an instrument

You have three ways to create a new booking.
1.	You can book your instrument by clicking on its name, you will have in the right window a reservation section.

    ![10](img/10.png)

    ![11](img/11.png)

    You can select the date and time of your booking and also create a recurrent booking in the Periodicity section.

2.	You can also directly access the reservation window by clicking on the “Calendar” icon.
    ![12](img/12.png)

3.	The other way of making a new reservation is in the “Reservation” – “Agenda” section.

    ![13](img/13.png)

    In the “Type” field, select Equipment. You can filter the equipment either by your Favorites and/or by using the different Conditional criteria.

    ![14](img/14.png)

    In “Bookable item”, you can write the name of the equipment and you will have drop down list of corresponding equipment.

    ![15](img/15.png)

    After you select the date of the booking (steps 1 to 3), click on the raw of your equipment. The “New reservation” side panel will open on the right. You can now create your booking and set up a Periodicity if needed.

    **Step 1**

    ![16](img/16.png)

    **Step 2**

    ![17](img/17.png)

    **Step 3**

    ![18](img/18.png)

    **Step 4**

    ![19](img/19.png)
    ![20](img/20.png)
    ![21](img/21.png)

    By Clicking on the name of the equipment, you can have a weekly overview of the bookings for this equipment. You can also create a booking from here, by clicking on the calendar.

    ![22](img/22.png)

## How to edit or cancel a booking

You can edit a booking by clicking on the time period. The right window will be displayed.

![23](img/23.png)

![24](img/24.png)

Click on “Edit” and you will be able to change the date and time. If you click on “Cancel”, the entire booking will be cancelled.

![25](img/25.png)

You can also edit or cancel your booking in the “Reservations” – “ Reservation” section. Click on the equipment name and the right window with your booking information will be displayed.

![26](img/26.png)

![27](img/27.png)

## How to access the booking a lab (location)

In some cases the equipment reservation is organized via lab booking. If you want to reserve a lab please select under Reservations “Locations” instead of “Equipment” as Type. The further way of proceeding is similar to described above for Equipment reservations.

![29](img/29.png)


## How to access the booking history

You can have an overview of all the bookings in the “Reservations” – “Reservations” section. You can filter them by date, type (Location or Equipment), name (Bookable item) and person who have made the booking (Reserved by).

![28](img/28.png)


## How to request booking configuration change or report an issue with Quarks?

If you wish to make one equipment bookable or make it 24 hours bookable [please send a ticket](https://service.uni.lu/sp?id=sc_cat_item_lsmc&sys_id=1dac368d54be72000a5374545c1c6a62&business_service_id=014ebe3ddb6a14148bcbf9b41d96195e&action_id=ec38cb7ddbaa14148bcbf9b41d9619a3). 

For all issues related to Quarks [please send a ticket](https://service.uni.lu/sp?id=bs_dep_catalog_entry&sys_id=0840cf71dbaa14148bcbf9b41d9619cb).

