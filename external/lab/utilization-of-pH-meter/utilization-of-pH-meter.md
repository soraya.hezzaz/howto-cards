---
layout: page
permalink: /external/lab/utilization-of-pH-meter/
shortcut: lab:utilization-of-pH-meter
redirect_from:
  - /cards/lab:utilization-of-pH-meter
  - /external/cards/lab:utilization-of-pH-meter
---

# Utilization of pH meter

## Routine Maintenance of pH meter “Seven Easy” and “Seven Compact” from Mettler-Toledo.

## 1. Description

<div align="center">
<img src="img/../description_pH-meters.jpg" height="300">
</div>

## 2. Personal protective equipment

Always wear a lab coat and gloves when working with the pH meter. Wear safety glasses if necessary.

## 3. Prepare the calibration reagents

1. The calibration cannot be performed using the reagents directly in their original containers, they need to be aliquoted.

2. Transfer a few millilitres of each of the three calibration solutions in a 15 mL falcon tube. Label them with the pH value of the solution and the date of aliquot.
3. Indicate the opening date on the original bottles and store them in the dark.

4. Keep the aliquoted solutions for a month, then replace them with fresh ones.

5. Indicate the date of exchange of the solutions in the log book.

## 4. Calibration

Calibration should be performed only if it has not been performed on the same day (see logbook) or if you have doubts about the calibration status of the instrument.
The pH meter allows for calibration of 1, 2 or 3 points.
The calibration standard solutions are of pH 4, 7 and 10.

1. Rinse the electrode with deionized water, wipe carefully the outside of the electrode with a lint free tissue.

2. Place the electrode in a calibration standard and press “CAL”.

3. The pH meter automatically endpoints when calibrating.

4. For 2 points calibration, repeat steps 1 to 3 for the second calibration standard.

5. For 3 points calibrations, repeat steps 1 to 3 with the third calibration standard.

6. Rinse the electrode with deionized water, wipe carefully the outside of the electrode with a lint free tissue.

7. After calibration, to return to sample measurement, press on:

    a. “Read” for the Seven Easy
    b. “End”, then “Save” for the Seven Compact

8. Add the calibration date in the equipment logbook.

## 5. Measuring the pH

1. pH is temperature sensitive, therefore, make sure that your solution is at the temperature you will use it (for example: 37°C for cell culture medium).

2. Make sure the sample has been thoroughly mixed.

    a. If you use magnetic stirrer, make sure you stop the stirrer before inserting the probe.

3. Rinse the electrode with deionized water, wipe carefully the outside of the electrode with a lint free tissue.

4. Place the electrode in the sample and press on “READ” to start the measurement.

5. During measurement, the decimal point of the display flashes.

6. Once the measurement is complete, the display freezes automatically.

7. Rinse the electrode with deionized water and place it back in its storing solution container (a 15 mL falcon containing 5 mL of 3 M KCl for example).

8. Adjust the pH of your solution, use a Pasteur glass pipet if you use strong acids or bases.

## 6. Maintenance

1. All calibration solutions should be kept away from the light.

2. Any spillage has to be cleaned immediately. Use wipes to dry the spill, then clean with water.

3. The electrode has to be maintained in 3 M KCl.

    a. Do not allow it to dry.
    b. Change the solution at least monthly.
    c. Indicate the date of change in the log book.
    d. If the filling solution is encrusted outside the electrode, remove it by rinsing the electrode with deionized water.

4. Contact the quality assistant if the logbook is full.

5. Would you notice any issue with the instrument, create a ticket.
