---
layout: page
permalink: /external/lab/lab-coats/
shortcut: lab:lab-coats
redirect_from:
  - /cards/lab:lab-coats
  - /external/cards/lab:lab-coats
---

# Lab coats

Which lab coat should I wear? Where do I get a new lab coat and where do I dispose a dirty one?


## Wear the right lab coat in the right area

<div align="center">
<img src="img/1.png">
</div>

### Lab coat mandatory

<div align="center">
<img src="img/2.png">
</div>


- All BSL1 and BSL2 laboratories
- Storage Rooms : Chemical Storage/ cryostorage/LCSB stock For all users, visitors, technicians. 

### Lab coat forbidden

<div align="center">
<img src="img/3.png">
</div>

- Offices and open offices
- Kitchen, Coffee Points (red flooring)
- Toilets

### Lab coat allowed

<div align="center">
<img src="img/4.png">
</div>

- Lift 
- Staircases 
- Corridor to the stairs/lift
- Corridor to the support team zone BT2 1st floor (trolleys/ fridges) __BUT not the support team offices__

## Where can i find a new clean lab coat?

### BT1

Clean lab coats are stored in the cupboard behind the Support Biotech Team desk, on the 2nd floor. They are sorted by Biosafety Level (BSL1 and BSL2) and sizes. For the larger sizes, you can find them with extra lengths on the sleeves (+15cm).

### BT2

Clean lab coats are stored in the cupboards of the MUF room, on the 1st floor, close to the Support Biotech Team offices. They are sorted by Biosafety Level (BSL1 and BSL2) and sizes. For the larger sizes, you can find them with extra lengths on the sleeves (+15cm).

In addition to the lab coat, it is strongly recommended to __wear safety goggles__ to protect your eyes. You can find some in the lab coat cupboards or in the common stocks (2nd floor for BT1, -2 floor for BT2).

<div align="center">
<img src="img/5.png">
</div>

<div align="center">
<img src="img/6.png">
</div>

## What should I do whith my dirty lab coat?

Please put your lab coat in the white plastic box close to the vending machine on the ground floor in BT1 or BT2. Someone from the Support Biotech Team will take care to send the lab coats for cleaning.

## Badges
To identify your lab coat as yours, do not forget to pin your badge on it. 

## Help needed?

If you need a new badge, or if you have any question regarding lab coats, goggles or other PPE, [please send a ticket](https://service.uni.lu/sp?id=sc_cat_item_lsmc&sys_id=1dac368d54be72000a5374545c1c6a62&business_service_id=2d0be5b3db500f40b0b4f9851d9619ac&action_id=689b61f3db500f40b0b4f9851d9619f8).

