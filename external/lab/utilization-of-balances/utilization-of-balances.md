---
layout: page
permalink: /external/lab/utilization-of-balances/
shortcut: lab:utilization-of-balances
redirect_from:
  - /cards/lab:utilization-of-balances
  - /external/cards/lab:utilization-of-balances
---

# Utilization of balances

## Routine maintenance of analytical balances from Mettler-Toledo XS4002S and XPE205DR


### Personal protective equipment

* Always wear a lab coat and gloves when using the balances.
* If necessary, wear safety glasses.
* If weighing dangerous, volatile compounds, use a balance located under a chemical hood.

There are such balances on the 5th floor in BT1 and on the 2nd floor in BT2.

### Weighing 

**XPE205DR: used for 1.4 mg to 220 g**

<div align="center">
<img src="img/XPE205DR.jpg" height="400">
</div>

**XS4002S: used for 800 mg to 4.1 kg**

<div align="center">
<img src="img/XS4002S.jpg" height="400">
</div>

1. For XPE balances, check that the air bubble is in the middle of the circle.
   It is located on the rear right foot of the instrument.
   <div align="center">
   <img src="img/XPE205DR_2.jpg" height="400">
   </div>
2. Switch on the balance by pressing on “On/Off” until the display appears.
3. In order to allow an equilibration of electronics within the balance, allow it to stand for 10 to 15 minutes before starting using it.
4. Zero the balance by pressing on “0”.
    a. For the XPE balance, make sure the doors are closed by pressing on “↨” or by passing your hand over the right and left upper parts of the terminal.
5. Place an anti-static weighing boat or a weighing paper in the middle of the pan of the balance. Do not place the material to be weighted directly on the pan.
6. If you weigh directly in a tube, place the Ergo Clip   on the pan of the balance and place your tube in it.

**Ergo clip**:

<div align="center">
<img src="img/Ergo-Clip_2.jpg" height="400">
</div>

7.	Tare the balance by pressing on “T”
8.	Place the material to be weighted in the anti-static weighing boat or on the weighing paper in the middle of the pan to avoid corner-load error, for the XPE balance,close the doors after loading your material.
9.	To prevent contamination of stock material, do not return unused substance to the stock bottle.
10.	After use, switch the balance off by pressing on the “ON/OFF” button.
11.	Contact the Quality Assistant if the logbook is full.
12.	If you notice anything wrong with the instrument, contact the instrument responsible.

### Cleaning

1. After each use, the balance and the material used to weight have to be cleaned.
2. **XPE205DR**:
    * Open the doors and remove the weighing pan
    * Wipe the surfaces with a brush
    * Wipe the weighing pan and the inside of the chamber with a damp cloth
    * Place the weighing pan back and make sure it is correctly placed
3. **XS4002S**:
    * Wipe the surfaces with a brush
    * Wipe the weighing pan with a damp cloth
    * If some powder fell off the pan, carefully remove the crown around the pan and clean it to remove any residue. If you need assistance, contact the instrument responsible person
    <img src="img/Crown.jpg" height="400">
    * When the crown is removed, carefully wipe with a damp cloth around the weighing pan
    * Carefully place the crown back
    * Always wipe the area around the balance with 70 % ethanol to remove any substance
    * Spoons: wash them with a mild detergent and water
