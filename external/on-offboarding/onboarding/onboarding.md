---
layout: page
permalink: /external/on-offboarding/onboarding/
shortcut: on-offboarding:onboarding
redirect_from:
  - /cards/on-offboarding:onboarding
  - /external/cards/on-offboarding:onboarding
---
# Onboarding new members at the LCSB

## Purpose
The LCSB regularly welcomes newcomers who have just joined one of the research groups or the support teams.
The on-boarding process aims to ensure their smooth insertion within their group and within the LCSB.
It completes the introduction at the Welcome Day of the university by providing information specific to the LCSB.
On top of practical information, this process should convey the LCSB values and the way we all work together.


## 1. Godparents
When a new member joins the LCSB, his team should assign him a godparent who will guide him during the on-boarding process.
The group PI are asked to nominate a senior member of the group as “god-parent”.
The godparent should show the newcomer around the LCSB premises, introduce them to all the relevant LCSB staff (help for networking during social events), and take care of some of the early procedures such as:
- prepare a desk
- get a computer
- get phone
- show how to get office supplies
- register for mail lists
- explain Uni/LCSB intranet, services and [important links](https://howto.lcsb.uni.lu/?general:links)
- introduce to the team
- explain the lunch voucher system and where to get food


## 2. Welcome meetings
- **University Welcome Day**: The newcomers have to attend the University Welcome Day to get a general overview of the university and have their official picture taken for the website.
- **LCSB Welcome meeting**: Once a month a two hours meeting is organised at the LCSB for the members who arrived recently. During the first part, a round table allow participants to introduce themselves and to place their “location sticker” on the LCSB map in BT2 staircase. Then, a member of the communication team presents the LCSB, its goals and values, and provides some practical information (who’s who, basic procedures, where to find answers, etc.). The second part is dedicated to security and safety (building evacuation, emergency exits, lab safety…).
The participants also get a copy of the LCSB annual and research reports and the slides used during the meeting are shared. The newcomers receive a calendar invitation for this meeting, sent by the communication team.

In order to introduce the newcomers to the whole LCSB team, the new members are mentioned in the LCSB newsletter (Photo, name, position and email address in the “Newcomers” section). They can also introduce themselves during the weekly team meeting at the LCSB.

## 3. Checklist for administrative tasks

- Get entrance badge at the Maison du Savoir (Security office on 2nd floor). Check with your team secretary what kind of access you have (rooms and hours) and how to update it if needed.
- Get acquainted with your colleagues, especially your group, the group secretary and the administrative team. The godparent should show the new employee around and introduce them to the LCSB team.
- Get access to the relevant mail lists (LCSB-Team + group list). The godparent should open a ticket in ServiceNow mentioning the name and the email address of the new employee and the required email lists that they should be added to.
- Familiarise with different LCSB tools: ATLAS server (to get access, [open a ticket in service now](https://service.uni.lu/sp)), the regular meetings (team meetings, TGIaF, CETs), the lunch vouchers distribution, etc. The godparent should explain how things work at the Uni/LCSB (policies and culture).
- Fill in the profile on [LCSB intranet](https://intranet.uni.lux/the_university/lcsb/lscb_internal/). Check that the information on the personal webpage on university website is accurate and that the newcomer is mentioned in LCSB people list and in the group member list on the website.
- Set up the proper email signature and confidentiality disclaimer for your university mailbox: [Guidelines on the intranet](https://intranet.uni.lux/the_university/sc/Lists/Policies%20%20Procedures/DispForm.aspx?ID=9)
- Order business cards through your group’s secretary if needed.
- Read the biweekly LCSB newsletter and explore the intranet to stay up to date with what is happening at the LCSB. Send an email to the LCSB communication team if the newcomer hasn’t received it after a few weeks.
- Complete your trainings to get further access to the labs. The godparent should make sure that the new colleague receives the Biosafety, Data Management and Data Protection Training before starting lab work, as all LCSB employees must be aware of the guidelines and risks.
