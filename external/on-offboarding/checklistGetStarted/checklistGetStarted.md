---
layout: page
permalink: /external/on-offboarding/checklistGetStarted/
shortcut: on-offboarding:checklistGetStarted
redirect_from:
  - /cards/on-offboarding:checklistGetStarted
  - /external/cards/on-offboarding:checklistGetStarted
---
# Newcomer's checklist

### Welcome to the LCSB!
Below you can find a to-do-checklist and some practical information aiming to guide your through the first days at the LCSB together with the (corresponding people in charge) in brackets.

### To Do's
* Sign your contract (HR department)
* Attend the University Welcome Meeting: Have your picture taken for our website and get a library card for online access of publications
* Attend the LCSB Welcome Meeting: Administrative welcome presentation (communication team) & Biosafety introduction (safety team)
* Fill in the newcomer's profile for your introduction on the intranet and in the newsletter (communication team)
* Get introduced to your team members, especially to the administrative team (your godparent)
* Have your E-Mail adress added to the relevant LCSB mailing lists (your godparent)
* Read the biweekly LCSB newsletter and explore the [LCSB Intranet](https://intranet.uni.lux/the_university/lcsb/lscb_internal/Pages/default.aspx)
* Hand in you tax card to the HR once you received it by post (HR department)
* Get your entrance badge (your team's secretary)


### Services and Websites
You will need two accounts to make use of services and tools:
* **UNI-LU account:** To use the IT services, also known as SIU, will give you your account at your first working day
* **LUMS account:** This account is used for particular services (see below) and will be created after you have your UNI-LU account. Your godparent can assist you in that.


#### The **UNI-LU account** is used to access the following services:

* Mail services, including web mail accessible from the browser via: [https://owa.uni.lu](https://owa.uni.lu)
* Microsoft Office & Teams: [https://portal.office.com](https://portal.office.com)
* University’s Service Portal to create tickets for IT, office setup, and other issues: [https://service.uni.lu](https://service.uni.lu)
* SAP, to make leave requests, see and print payslips and other HR-related services: [https://fiori.uni.lu/fiori](https://fiori.uni.lu/fiori)
* Webex which is used for all virtual work meetings (implemented due to the COVID-19 pandemic), unless the other party requires the use of other software. You can login with UNI-LU account at: [https://unilu.webex.com/](https://unilu.webex.com/)
* University Intranet: [https://intranet.uni.lux](https://intranet.uni.lux)
* LCSB Intranet: [https://intranet.uni.lux/the_university/lcsb/lscb_internal/Pages/default.aspx](https://intranet.uni.lux/the_university/lcsb/lscb_internal/Pages/default.aspx)
* specific communication channels, such as Slack, Basecamp an others, depending on your team. Your godparent will provide you with further information.


#### The LUMS account is used to access the following services:

* Owncloud, a file cloud deployment at LCSB: [http://owncloud.lcsb.uni.lu](http://owncloud.lcsb.uni.lu)
* Gitlab, where the bioinformatics core team we manages its work products, source code and websites: [https://gitlab.lcsb.uni.lu/](https://gitlab.lcsb.uni.lu/)
* other specific and licence-dependent software that is relevant for your work


#### Enjoy your time at the LCSB!
If you have any questions, do not hesitate to ask!

