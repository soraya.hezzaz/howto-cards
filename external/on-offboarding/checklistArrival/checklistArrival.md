---
layout: page
permalink: /external/on-offboarding/checklistArrival/
shortcut: on-offboarding:checklistArrival
redirect_from:
  - /cards/on-offboarding:checklistArrival
  - /external/cards/on-offboarding:checklistArrival
---
# Checklist upon arrival in Luxemburg

This checklist for newcomers will provide you with some practical advice what to take care of before arriving in Luxembourg. The pdf document for download can be found [here](https://intranet.uni.lux/the_university/lcsb/lscb_internal/LCSB%20Handbook/Appendix%20files/Newcomer%20Checklist%20upon%20Arrival%20in%20Luxembourg.pdf).

### Welcome to Luxembourg!
Arriving in a new country means also dealing with bureaucracy. Here we would like to give you a first overview of the things that you need to do upon arrival.

### Living in Luxembourg
- Register at your [local commune](https://www.syvicol.lu/fr) ("Hotel de Ville") within
    - 3 days of arrival for all non-EU citizens
    - 8 days for EU citizens staying longer than 3 months
Take with you your passport, renting contract of accommodation, certified
birth certificate and work contract. After registration you will receive a
paper card that states on which address you have been registered. At the
same time you can also register your drivers license (only for EU citizens)
##### Within 3 months:
- Register your vehicle. For more information [check here](https://www.euraxess.lu/).
- Apply for your residence permit (only for non-EU citizens)
##### Within 1 year:
- Apply for a [Luxembourg driving license](https://www.euraxess.lu/) (only for non-EU citizens)

### Children
- Register children at their school

### Money
- Open a bank account and order credit/debit cards. Be aware that it can
take up to 3 weeks until you receive those bank cards.
- Take with you your passport, work contract and the paper card you got from the commune when registering.

<!--Process owner: Comm'department-->

