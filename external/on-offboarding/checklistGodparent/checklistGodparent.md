---
layout: page
permalink: /external/on-offboarding/checklistGodparent/
shortcut: on-offboarding:checklistGodparent
redirect_from:
  - /cards/on-offboarding:checklistGodparent
  - /external/cards/on-offboarding:checklistGodparent
---
# Checklist for godparents

Thank you for accepting thegodparent role and taking the time to help him/her get started at the LCSB!
Below you can find a checklist for the first steps with this task together with the (corresponding people in charge) in brackets.
The overview checklist in .pdf-format ist avaliable [here](https://intranet.uni.lux/the_university/lcsb/lscb_internal/LCSB%20Handbook/Appendix%20files/Godparent%20Checklist%20-%20newcomers.pdf).

### Tasks for the godparent

- Welcome the new employee
- Introduce the new employee to the administrative staff in order to provide him/her with:
    * an entrance badge (your team's secrretary)
    * office supplies (communications team)
    * a computer (your team's secrretary)
&nbsp;
- Show the new employee around and introduce him/her to your group and to the LCSB team
- Make sure that the new colleague receives and signs the Biosafety Information Pack before starting lab work, as all LCSB employees (also administartive staff) must be aware of the guidelines and risks of the labs.
- Explain and show:
    * LCSB intranet and newsletter and UL intranet
    * Uni/LCSB policies and culture: how to find details about them on the intranet
    * ATLAS server structure
    * Meetings at LCSB: team meeting (mandatory!), group meetings, TGIF, CETs etc.
    * Infrastructure: transport, lunch, chèques repas etc.
    * email signature and confidentiality disclaimer
    * business cards (to order them contact your group’s team secretary)
&nbsp;
- Make sure the new employee is added to the relevant email lists. For this please [open a ticket at ServiceNow](https://service.uni.lu/sp), mentioning the name and **email address of the new employee** and the **email addresses of the required email lists** that he should be added to.
- For details, see the LCSB Handbook on the intranet.

### Thanks for your help!
