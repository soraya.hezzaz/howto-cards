---
layout: page
permalink: /external/on-offboarding/godparent/
shortcut: on-offboarding:godparent
redirect_from:
  - /cards/on-offboarding:godparent
  - /external/cards/on-offboarding:godparent
---
# Godparents for Newcomers
Please also consult the [checklist for godparents](https://howto.lcsb.uni.lu/?on-offboarding:checklistGodparent).

### Summary
In order to help new employees to easily integrate in their new workplace and senior members to better know and understand the new colleague, each new employee will be assigned a "godparent".

### What is a godparent?
Being a godparent is a voluntary function within the LCSB.
A godparent is as a person who, through its experience at the centre and in Luxembourg has gained an extensive knowledge about procedures, rules, corporate culture etc. and who desires to help others, advice new colleagues when they arrive in Luxembourg and who desires to build new relationships.
### What should a godparent do?
A godparent is free to organise and build the relationship with a new employee. However, the following task should be covered.
* Welcome the new employee
* Show the new employee around and introduce him/her to the team and to other LCSB/ Uni.lu members: most importantly to the administrative in order to provide a badge, office supplies…
* Make sure that the new colleague receives and signs the Biosafety Information Pack, as all LCSB employees must be aware of the regulations and risks of the labs
* Explain Uni/ LCSB policies and culture
* Make sure the new employee is added to the relevant email lists. For this please [open a ticket at ServiceNow](https://service.uni.lu/sp), mentioning the name and **email address of the new employee** and the **email addresses of the required email lists** that he should be added to.
* Make sure the newcomer gets access to ATLAS as appropriate.
Duration of godparenting: Minimum 2 months and no longer than 6 months.
### How to become a godparent?
All LCSB members are eligible to become godparents. There are no minimal qualifications required, mentors must only work for at least 6 month at the Uni.
However, in general HR in collaboration with the LCSB’s administration team will shortlist few potential candidates. Potential candidates shall be contacted and asked if they accept the function. In a case of refusal the next candidate on the list will be contacted.
Preferred candidates for this assignment are members of the same unit or project, a relevant research field, or the same hierarchical level. To avoid conflicts of interest, in principle PI or superiors are to be avoided as godparents.

