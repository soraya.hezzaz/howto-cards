---
layout: page
permalink: /external/on-offboarding/checklistBeforeArriving/
shortcut: on-offboarding:checklistBeforeArriving
redirect_from:
  - /cards/on-offboarding:checklistBeforeArriving
  - /external/cards/on-offboarding:checklistBeforeArriving
---
# Newcomer Checklist before Arriving in Luxembourg

This checklist for newcomers will provide you with some practical advice what to take care of before arriving in Luxembourg. The pdf document for download can be found [here](https://intranet.uni.lux/the_university/lcsb/lscb_internal/LCSB%20Handbook/Appendix%20files/Newcomer%20Checklist%20Before%20Arriving%20in%20Luxembourg.pdf).

### Entering Luxembourg
- Check the validity of your passport!
- Check your entry and residence conditions! Depending on your nationality you will need a **visa and/or residence permit**. Detailed information on who needs such permits and which documents are required can be found [on the euraxess](https://www.euraxess.lu/) homepage.
- Depending on your nationality you will need a **work permit**. HR will take care of this procedure. They will send you the necessary documents that you need to fill in.


### Getting to Luxembourg
- To avoid extra expenses, only book your flight once you know the first working day of your employment and that you got a visa.
- Luxembourg itself has a small **airport** called Findel in Luxembourg City. Alternatively, one could fly to airport Frankfurt Hahn or Charleroi where low budget airlines are travelling to. There is a direct cheap [bus connection](https://www.flibco.com/en) between those airports and Luxembourg.
- Please check the Luxembourgish [railway website](https://www.cfl.lu) for national and international train connections. To get to the LCSB get off at station Belval Université. Detailed information on how to find the LCSB building can be found [on the university website](https://wwwen.uni.lu/lcsb/contact%20).

### Health
- All LCSB employees will be covered by CNS health insurance that covers up to 2/3 of your health expenses. To cover the rest of the expenses every employees is free to arrange private insurance at own expense. For details on health insurance and social security check the [Daily Life section of euraxess](https://www.euraxess.lu/node/174962/).


### Looking for accommodation
- In Luxembourg most apartments are rented out by estate agencies. It’s quite common to pay 1 month rent as agency fee and up to 3 months rent as deposit to the landlord. Euraxess provides [further information and compiled a list of useful websites](https://www.euraxess.lu/luxembourg/information-researchers/accommodation) for this.
- Alternatively, it is also possible to apply for university housing: PhD students can rent student accommodation for their entire stay at the UL. More information can be found [on the university website](https://wwwen.uni.lu/students/accommodation). Researchers can apply for a short- term stay (1 to 3 months) in university housing. [This file](http://wwwen.uni.lu/content/download/56478/668845/file/Housing%2520procedure%2520and%2520online%2520application.pdf%20) provides further information on this.


### Tax
- You will receive your tax card automatically per post after the University of Luxembourg has registered you at the social security system. Hand this tax card to HR. They will take care of the rest.
- General information on taxation can be found [on the euraxess website](https://www.euraxess.lu/).


### Children
- The LCSB has partnered with a nursery in Esch-sur-Alzette. For more information, please contact: mike.hansen@uni.lu
- If you are accompanied by children bring their school reports. Ceck [the euraxess website](https://www.euraxess.lu/) for more information about the Luxembourgish school system.



### Money
- Please be aware that it can take up to 3 weeks after opening a new bank account until you receive your new banking cards. So make sure you prepare other means to bridge this period.

If you have any questions, don’t hesitate to contact us. We are looking forward to welcoming you in person.
**See you soon at the LCSB!**
