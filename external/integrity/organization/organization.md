---
layout: page
permalink: /external/integrity/organization/
shortcut: integrity:organization
redirect_from:
  - /cards/integrity:organization
  - /external/cards/integrity:organization
---

# Organization

## Process of organizing files and folders

The present suggestions are to guide individual researchers and staff members. It is important to note that certain research groups already have specific group guidelines in place.

## Structure of folders

Organizing files in directories is a key task to stay organized and to make sure that everyone is
able to find back the files easily.

A key rule is to name the folders with straightforward but explicit names, and to include the decision tree of where to put a given file inside the folder structure. In fact, the directory tree should in some sense
reflect the decision tree.

More details on naming files and folders are provided in the dedicated [file naming card]({{ '/?integrity:naming' | relative_url }}).

## Suggestions

There are a few suggestions to organize a folder structure:

### Suggestion 1
Only have a few folders per level (i.e. 5-6 folders per level). That way, the decision of where to include a file is straightforward.

### Suggestion 2
The more general the upper levels and the more precise the lower levels are, the easier it is for someone to decide where to include a respective file. The idea is to mirror a real tree.

### Suggestion 3
Depending on the setup of the research group, it might be useful to structure the group's overall mission into separate programs, sub-missions, or areas of expertise.

### Suggestion 4
Archived folders (or frozen folders) can be named with an `_` (underscore) preceding the actual name of the folder.

## Example

An example for a folder of research group at LCSB:

```
LCSB_GRPNAME
├── _alumni
│   ├── firstLast1
│   └── firstLast2
├── grants
│   ├── grant1
│   └── grant2
├── programs
│   ├── program1
│   │   ├── _project0
│   │   ├── project1
│   │   │   ├── papers
│   │   │   │   ├── paper1
│   │   │   │   │   ├── analysis
│   │   │   │   │   ├── figures
│   │   │   │   │   ├── manuscript
│   │   │   │   │   └── rawData
│   │   │   │   └── paper2
│   │   │   │       ├── analysis
│   │   │   │       ├── figures
│   │   │   │       ├── manuscript
│   │   │   │       └── rawData
│   │   │   ├── posters
│   │   │   └── presentations
│   │   └── project2
│   └── program2
├── resources
│   ├── courses
│   ├── hardware
│   ├── literature
│   ├── personnel
│   └── software
├── students
│   ├── firstLast1
│   └── firstLast2
└── team
    ├── achievements
    └── meetingNotes
```

## Specific guidelines for raw data

In the `rawData` folder, of each paper, you should include
* Links to data
* Links to lab books (Paper)
* PDF of book if ELN

When using data collection platforms, make sure that you follow their specific guidelines. Avoid the use of personal folders for raw data storage.

Generally, the following is often the case depending on the type of data:

* **Common data**: Export raw data where possible and store it on one of the storage locations defined above or as defined in the group guidelines.
* **Group specific data**: Export raw data where possible and store it on one of the storage locations defined above or as defined in the group guidelines. Make sure all raw data is properly backed up.
* **Platform specific data**: Export or retrieve raw data where possible and store it on one of the storage locations defined defined above, as defined in the group guidelines, or as defined in the platform-specific guidelines. No backups will be provided by the platform.

## Specific guidelines for ELN

In general, make sure that all lab books (paper and electronic versions) exist and are available to somebody from the group. By default, all ELN should be readable by the group.
