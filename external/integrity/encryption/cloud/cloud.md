---
layout: page
permalink: /external/integrity/encryption/cloud/
shortcut: integrity:encryption:cloud
redirect_from:
  - /cards/integrity:encryption:cloud
  - /external/cards/integrity:encryption:cloud
---

# Data upload to cloud

Data encryption and hash-check for uploading to UL-LCSB onedrive or owncloud from Windows.

| Requirements | Restrictions |
| ------ | ------ |
| Windows 8 or newer (recommended Windows 10) | Recommended for data size less than 100 GB |
| RAM 2GB (recommended 4GB) | If transfer through Aspera is not possible |
| Enough free disc space (data will be duplicated for encryption) | Larger data will take more time to encrypt |
| Administrative rights to install software |  |
| Web browser (Firefox, Edge or Chrome) |  |

## Install `7zip` for encryption

Download and install `7zip` from [here](https://www.7-zip.org/)

## Install tool for MD5 checksum calculation

1. Search Microsoft Store for `Simple Checksum Calculator` or go to this [link](https://www.microsoft.com/en-us/p/simple-checksum-calculator/9nblggh5l6t9?activetab=pivot:overviewtab)
2. Install `Simple Checksum Calculator`


## Encrypt your data with `7zip`

1. Right click on the folder to encrypt > 7zip > Add to archive
2. Select following values:
   - Archive format: `zip`
   - Compression: `store`
   - Check `Show Password` box
   - Encryption: `AES256`
3. Enter a password and make a copy of the encryption password :key:
4. Click save to save the encrypted zip file

![](media/7zip-encryption-windows.mp4?width=400)

## Calculate the MD5 checksum with `Simple Checksum Calculator`

1. Open `Simple Checksum Calculator` from the start-menu
2. Select `File` tab from the top menu
3. Click on the file icon box in the left panel and load the encrypted zip file
4. Make a copy of the MD5 checksum key :label:

![](media/Get_MD5_checksum_windows.mp4)

## Data upload

1. Open the `OneDrive` or `ownCloud` URL link that you have received from UL-LCSB contact
2. Upload the encrypted zip file
3. After the upload completion, open [UNILU privatebin](https://hpc.uni.lu/privatebin/)
4. Paste the **encryption password :key:** and **MD5 checksum :label:** in the Editor box and press send
5. Copy the privatebin URL and send it your UL-LCSB contact through email