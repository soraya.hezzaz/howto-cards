---
layout: page
permalink: /external/integrity/encryption/file/
shortcut: integrity:encryption:file
redirect_from:
  - /cards/integrity:encryption:file
  - /external/cards/integrity:encryption:file
---
# Encrypting Files and Folders
Encryption is an effective measure to protect sensitive data. In this lab card we provide instructions for file/folder encryption on platforms commonly used by LCSB staff.

**IMPORTANT NOTICE:** One important requirement of using Encryption is that you must  manage your Encryption Passwords/Keys. Failing to do so will mean **loosing your data indefinitely**. In this [link]({{ '/?access:passwords' | relative_url }}) we list  tools that can be used for password management. **Please make sure you have arrangements for password management before starting the encryption of data**.

## macOS
The built-in mechanism for file-level encryption on a Mac is  Encrypted Disk Images (*.dmg* files). In order to create a disk image:

 1. Using Finder open up  **/Applications/Utilities/Disk Utility**
 2. From the Disk Utility menu select **/File/New Image** <br/> ![Alt](img/enc_mac_1.png "Title")
 3. You will have two options; either to create a **Blank Image** or to create an **Image from Folder**. Choose the option that fits your situation.
 4. You will be prompted for configurations for the image. <br/> ![Alt](img/enc_mac_2.png "Title")
 5. Make the following settings
    - Set a name for your image (also set a size if this is a blank image),
    - Set a format for the image. Format should be _read/write_ for blank images. When creating an image from a folder to an image the format can be  _read/write_ or  _read_ only.
    - Turn encryption on by selecting either 128 or 256 bit option, you will then be prompted for a _password_ for your image. Provide a password and save.
 6. A _.dmg_ file will be created with the name you supplied. <br/> ![Alt](img/enc_mac_3.png "Title")
 7. Whenever you want to load the image you will be prompted for  the password. Remember to eject the disk image when you're not accessing the files within. <br/> ![Alt](img/enc_mac_4.png "Title")


## Linux
Make sure you have all relevant data in a single file. In case you have multiple files, put them in a folder and create a compressed archive (aka tarball).

```
tar cvzf your-compressed-file-name.tar.gz your-directory/
```

You can use the below command to encrypt a file on Linux.

```
gpg -c file_to_be_encrypted
```
You will be asked for a passphrase.

```
Enter passphrase:<YOUR_PASSWORD>
Repeat passphrase:<YOUR_PASSWORD>
```

The following command can be used to decrypt the file.

```
gpg  encrypted_file
```

Instead of using a passphrase, you can also encrypt files using an encryption key. You can use GPG to create an encryption key as follows.


```
gpg --gen-key
```

If the above command hangs for a long time, and complains about _entropy_ then run the following commands and then re-run key generation.

```
yum install rng-tools
rngd  -r /dev/random
```

## Windows
On Windows, file level encryption can be achieved using the Encrypting File System (EFS) feature. Note that EFS is only available on Windows 10.

In order to use turn on EFS for a folder:

1. Using **File Explorer** locate the folder you want to encrypt. Right click and select **Properties**.
2. Select **Advanced**. From the **Advanced Attributes** screen check the option **Encrypt contents to secure data** and click **OK**, then **Apply**. If this option is appearing dimmed or disabled please contact [University IT Support](https://service.uni.lu/). <br/> ![Alt](img/enc_win_1.png "Title")
3. When prompted select the option to apply changes (encryption) to  **subfolders and files** and click **OK**.
4. Notice that this process does not ask you for a password as the files are protected with a key enabled only when **you** login. When other users, including admins, login to your machine they will not be able to see the contents of encrypted folders/files.
5. When you enable EFS on your machine Windows will start prompting you to backup your **encryption key**. It is advised to backup, as you may not be able access encrypted folders after a Windows re-install.
6. When prompted for backup, choose **Backup now**. This will take you to the **Certificate Export Wizard**. for the export format select **Personal Information Exchange (.PFX)** also select **Enable certificate privacy**. <br/> ![Alt](img/enc_win_2.png "Title")
7.  In the **Security** step select **Password** and set a password on the encryption key.
8.  In the final step navigate to the location you want the key backup (the _.pfx_ file) to be stored. This could be a USB drive or your personal ownCloud folder.

## Cloud Platforms

As per LCSB Policy, you should not store sensitive human data on commercial cloud services (e.g. Google Drive, Dropbox). However, there may be situations where commercial clouds are used:

- There is a project/consortium level agreement to use external cloud storage,
- You're working with sensitive data, and need to temporarily co-access it with research collaborators,
- You're working with non-sensitive data and using the cloud as a backup target.

In such cases, you may use the following desktop tools to encrypt cloud folders.

- [boxcryptor](https://www.boxcryptor.com/en/) (Paid). If you're holding sensitive LCSB research data  on commercial cloud (case 2 above), you must use Boxcryptor. Contact the LCSB IT  team to request a license.
    - [Installation/Mac](http://bit.ly/BXC-MAC_INSTALLATION)
    - [Installation/Windows](http://bit.ly/BXC-WINDOWS_INSTALLATION)
    - [Sharing Folders](http://bit.ly/BXC-FILE_SHARING)
    - [Encrypt Folder](http://bit.ly/BXC-FILE_ENCRYPTION)
    - [Decrypt Folder](http://bit.ly/BXC-FILE_DECRYPTION)

- [Cyberduck](https://cyberduck.io/cryptomator/) (Free).


# Coming Soon

The Uni-LU HPC Team is planning to install [EncFS](https://en.wikipedia.org/wiki/EncFS) on the HPC clusters. EncFS allows for the creation of an encrypted volume (similar to a folder). EncFS provides transparent encryption, once you mount the encrypted volume, anything that goes into the volume will automatically be encrypted. Also, whenever you try to view or process a file in a mounted EncFS volume, it will be decrypted for you (behind the scenes) automatically.

We will provide instructions for EncFS once it becomes available .
