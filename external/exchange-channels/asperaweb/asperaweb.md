---
layout: page
permalink: /external/exchange-channels/asperaweb/
shortcut: exchange-channels:asperaweb
redirect_from:
  - /cards/exchange-channels:asperaweb
  - /external/cards/exchange-channels:asperaweb
---
# AsperaWEB Quick Guide

{:toc}

## Overview

AsperaWEB is an IBM Aspera deployment at the LCSB-University of Luxembourg. AsperaWEB supports end-to-end encrypted data transfer and can handle high data volumes e.g. several tera bytes.

## Obtaining a AsperaWEB access link

You need an **access link** to use AsperaWEB. An access link is a temporary, password-protected space, much like a drop box, on LCSB's AsperaWEB server. In order to obtain an access link, you should contact your research collaborator at the LCSB-University of Luxembourg. Once created, you will receive your **access link** and associated **password** (link) by e-mail.

> **IMPORTANT NOTE: Whenever the LCSB shares a password for a AsperaWEB endpoint (collaborator), the password is transmitted via a link which will expire. Therefore you should make a record of your password once you view it.**

An access link can be reached via standard web browsers. Data can be transferred to/from an access link in two ways:

* Through the web browser by visiting the link, which is our recommended way of data transfer, described in this [section of the guide](#ASPERAWEB_WEB)
* Through the use of a command line tool. If your data sits in an environment, where you can not launch a web browser, then you may use use a command line client tool to reach an access link. This process is described in this [section of this guide](#ASPERAWEB_CLI).

The use of AsperaWEB is mediated by LCSB's data stewards. If you require assistance in using AsperaWEB, you should send an email to the [LCSB datastewards](mailto:lcsb-datastewards@uni.lu) or refer to the [Troubleshooting](#ASPERAWEB_TROUBLESHOOTING) section of this guide.

<a name="ASPERAWEB_WEB"></a>

## Accessing AsperaWEB via Web Interface

In the following steps we provide instructions on how to use AsperaWEB web interface.

1. Once you receive your **access link** and **password** from the LCSB, visit the link using a standard web browser. Firefox 66.x (or higher) recommended, but others should also work. You will be prompted for your password (see below).<br/>
 ![Alt](img/asperaweb_1.png "Title")

2. When you visit a AsperaWEB access link for the very first time, you will be prompted to install or update **IBM Aspera Connect** client.

* click **Download** or **Download the App** buttons (see below). <br/>![Alt](img/asperaweb_2.png "Title")

* wait for the download to finish, the prompt should go to step 3. <br/>![Alt](img/asperaweb_3.png "Title")

* open the installer just downloaded and start the installer. <br/>![Alt](img/asperaweb_4.png "Title")

* close the installer, the Aspera Connect should try to open - *depending on your Operating System you may be asked if you are sure to open it* -

* the prompt should disappear

* restart your browser

1. The **access link** page will display a **File Browser** section. Depending on the settings per access link, users can create or delete folders in the File Browser and upload and/or download data.<br/>
  ![Alt](img/asperaweb_6.png "Title")

2. Clicking **Upload** or **Download** will launch the **IBM Aspera Connect** client on your computer. You first will be asked whether you allow the client to connect to aspera.lcsb.uni.lu. Choose **Allow**.

3. At any time you can launch **IBM Aspera Connect** to display the status of uploads to or downloads from your computer. <br/>
  ![Alt](img/asperaweb_8.png "Title")

4. All data are encrypted on server side and they stay encrypted also upon download. For decryption, you have to navigate into your **IBM Aspera Connect** window and click "**Unlock encrypted files**". <br/>
  ![Alt](img/asperaweb_9.png "Title") <br/>
You will be prompted for encryption passphrase which is present on the file browser. <br/>
  ![Alt](img/asperaweb_10.png "Title") <br/>
Encrypted files are by default kept on your disc after decryption. If you want to change this behaviour, navigate to Options->Settings and check "Delete encrypted files when finished" box.

5. You can also navigate to the help section on the top right of the browser. It contains information and links to get support.

<a name="ASPERAWEB_CLI"></a>

## Accessing AsperaWEB via Command-Line Tool

Go to the help section of your access link

<br/> ![Alt](img/asperaweb_help_link.png "Title") <br/><br/><br/>

And follow the steps in the **Using the command line** section.

<br/> ![Alt](img/asperaweb_cli_tokens.png "Title") <br/><br/><br/>

<a name="ASPERAWEB_TROUBLESHOOTING"></a>
## Troubleshooting

You can use the official [IBM Aspera Diagnostic Tool](https://test-connect.asperasoft.com/) to troubleshoot your connectivity issues.

### **Using Microsoft Edge browser**

Microsoft Edge browser requires to download and install [IBM Aspera Connect for Edge](https://microsoftedge.microsoft.com/addons/detail/ibm-aspera-connect/kbffkbiljjejklcpnfmoiaehplhcifki).

### **UDP/TCP port and firewall**

> **IMPORTANT NOTE:** Aspera requires UDP ports to be enabled on firewalls.

Specifically your firewall should:

* Allow outbound connections from the Aspera client on the TCP port (TCP/33001, by default, when connecting to a Windows server, or on another non-default port for other server operating systems).
* Allow outbound connections from the Aspera client on the fasp UDP port (33001, by default).
* If you have a local firewall on your server (like Windows Firewall), verify that it is not blocking your SSH and fasp transfer ports (e.g. TCP/UDP 33001).

Detailed information on how to configure firewalls when working with Aspera is given [here](https://download.asperasoft.com/download/docs/p2p/3.5.1/p2p_admin_win/webhelp/dita/configuring_the_firewall.html).
