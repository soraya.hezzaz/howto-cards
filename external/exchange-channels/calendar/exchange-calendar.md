---
layout: page
permalink: /external/exchange-channels/calendar/
shortcut: exchange-channels:calendar
redirect_from:
  - /cards/exchange-channels:calendar
  - /external/cards/exchange-channels:calendar
---
# Sharing calendar in Microsoft Exchange

If you don't want to be overwhelmed by doodle polls and multiple rescheduling of meetings, you can share your calendar with your collaborators so they can see when you are busy.

## Sharing calendar via Outlook Web App (OWA)
  1. log-in into OWA ([owa.uni.lu](https://owa.uni.lu))

  1. navigate to calendar (top-left corner)
    <img src="img/an_owa_mail.png"  height="250px" />

  1. click on **Share**
    <img src="img/an_owa_calendar.png"  height="250px" />

  1. add people you want to share your calendar with
    <img src="img/an_owa_sharing-detail.png"  height="350px" />

  1. choose a role for each of your collaborators. This defines how much information related to your events will be visible to others.

  1. send the invitation to your calendar

## Sharing calendar via Outlook Client
In order to share calendar using Outlook Client:
  1. navigate to calendar tab
    <img src="img/an_outlook_mail.png"  height="300px" />

  1. click on **Share Calendar**
    <img src="img/an_outlook_calendar.png"  height="300px" />

  1. add your collaborators you want to share your calendar with as recipients

  1. choose amount of shared information about your events
    <img src="img/an_outlook_sharing-detail.png"  height="300px" />

  1. send the invitation to your calendar

