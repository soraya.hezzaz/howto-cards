---
layout: page
permalink: /external/exchange-channels/owncloud/
shortcut: exchange-channels:owncloud
redirect_from:
  - /cards/exchange-channels:owncloud
  - /external/cards/exchange-channels:owncloud
---
# Owncloud

**LCSB ownCloud**  is a private cloud storage service for the use of LCSB staff and collaborators. It is suitable for exchanging small-sized files (up to 1-2 gigabyte). All communication with the LCSB ownCloud server is SSL encrypted.


## Obtaining a LUMS account

A LUMS (LCSB User Management System) account is needed in order to use LCSB ownCloud. LUMS accounts for LCSB staff are normally created within the first few days of starting work at the LCSB. In addition to staff, researchers at Partner Institutes may be given LUMS accounts. If you want to create a LUMS account, or require support, you should contact [LCSB admin team](mailto:lcsb-sysadmins@uni.lu).

## Using LCSB ownCloud

Similar to other cloud storage systems, ownCloud is accessible both via a browser and also via a client application. On the web, LCSB's ownCloud is at [https://owncloud.lcsb.uni.lu/](https://owncloud.lcsb.uni.lu/)



![LCSB ownCloud web login](img/owcld_1.png)

You can download the ownCloud client suitable for your staff computer [here](https://owncloud.org/download/). User documentation on ownCloud tools and portal can be found [here](https://doc.owncloud.com/server/index.html).

When sharing research data, one should observe the following guidance using Owncloud:

* Limit folder shares to only the personnel that needs to access data.

* When sharing via Links, always set a **password** and an **expiration date** for the link.



  ![](img/owcld_2.png)


* When sharing folders via Links, **the link passwords should not be sent plain in email**. Instead, password links should be used. If you need to generate link passwords can use the [Private bin](https://privatebin.lcsb.uni.lu) service hosted by the LCSB.


 **IMPORTANT NOTE: Whenever the LCSB shares an ownCloud folder with you (collaborator), the share password will be reachable via link, which will expire. Therefore you should make a record of your password once you view it.**


## Installing Owncloud Desktop Client

**Important:** OwnCloud is fetching information from LUMS, therefore changes like a password reset (see How-to cards [Changing password for LUMS accounts]({{ '/?lums-passwords' | relative_url }}) should be performed **only** via LUMS!

1. Please try following link to download the owncloud desktop client depending on your operating system (this is an example for Windows, similar steps for other OS) -

    [OwncloudClient](https://owncloud.org/download/#owncloud-desktop-client)

2. Start the owncloud client installation by clicking twice on the downloaded file

3. Owncloud Setup Wizard will pop-up, like below image -

    <img src="img/setup-wizard-01.png" height="300px"><br/>

4. Click **Next**

5. Clic **Next** when Custom Setup window appears -

    <img src="img/custom-setup-01.png" height="300px"><br/>

6. Click **Install** on Ready to Install window -

7. Click **Yes** when asked if you want to allow this app to install software on your PC -

    <img src="img/user-account-control.png" height="300px"><br/>

**Note**- If you do not have admin rights, please ask SIU to provide you the admin rights on the laptop or do the installation for you.

8. Click **Finish** when the owncloud setup wizard completes -

    <img src="img/setup-wizard-complete.png" height="300px"><br/>

9. Please enter **owncloud.lcsb.uni.lu** when the following window appears -

    <img src="img/server-address.png" height="300px"><br/>

10. Enter User Credentials -

    * Username - firstname.lastname
    * Password - LUMS password

    <img src="img/user-credentials.png" height="300px"><br/>

11. It may ask you to trust the **certificate**, please do so by clicking **OK**

12. Once you are done with above steps, you should see something as below -

    <img src="img/local-folder-options.png" height="300px"><br/>

13. You can choose if you want the Owncloud folder in a different location by clicking on **Local folder** bar as shown in above image.

14. From above image, you can also **choose what to sync**, as you may not need all the folders downloaded on your latpop, which you do not need.

15. You can select on unselect folders and subfolders to your preference. The desktop client enables you to sync files from your desktop to the OwnCloud server. You can create folders in your home directory and keep the contents of those folders synced with your ownCloud server. You can also select the files/folders that you want to remove from your local file system.\
  **Important:** the file/folder will be still stored on the server!

16. Then select **connect** and you are done.

17. Once the synchronization is finished, you should see something as below -

    <img src="img/synchronization.png" height="300px"><br/>

18. From the activity you can see what was **synced**, **not synced** and **server activity**

    <img src="img/activity.png" height="300px"><br/>

19. On windows, the Owncloud bar is on the right hand corner under hidden icon **^**

20. For quick access to owncloud folder, please go to **Local Disk (C:)** > **Users** > **yourname** and then right click on the Owncloud folder and select **Pin to Quick access**

    <img src="img/quick-access.png" height="300px"><br/>