---
layout: page
permalink: /external/general/getToLCSB/
shortcut: general:getToLCSB
redirect_from:
  - /cards/general:getToLCSB
  - /external/cards/general:getToLCSB
---
# How to get to the Luxembourg Centre for Systems Biomedicine

![](img/LCSB.jpg)


### 1. Arriving by plane at Luxembourg airport:
The Luxembourg airport, called Findel, is 34 km away from the University Campus BELVAL where LCSB is located.
You can take either taxi or public transport to reach LCSB.
- **Via Taxi (110€ one way)**

From Luxembourg airport you can take a taxi to BELVAL near the city Esch-sur-Alzette.
Trustworthy taxi services: coluxtaxi (colux.lu; tel: +352 48 22 33); webtaxi (webtaxi.lu; tel: +352 27 515)
*Price: ca 110€ one way (in Luxembourg taxis are very expensive!)*
*Duration: 30 minutes (during rush hour 45-60 min)*

Benelux taxi: the price from/to airport is about 85€. To take advantage of this offer, the taxi has to be booked in advance.
www.beneluxtaxi.lu / +352 40 38 40 / reservation@beneluxtaxi.lu (please ask confirmation of the price)
A map of the Campus BELVAL and how to find both LCSB buildings is shown below.

- **Via public transport (free)**

From Luxembourg airport you can take the bus to Luxembourg central train station using bus line 29 or line 16 and
stop at "Central station (Gare Centrale)". For the timetable check the following links:
line 29: http://www.vdl.lu/autobus_ligne29.html
line 16: http://www.vdl.lu/autobus_ligne16.html

From Luxembourg’s central train station, you will get a train every 20 minutes to BELVAL-Université where LCSB
is located. Please see below "Arriving by Train" for all details.

*Public transportation is free in Luxemburg!*
*Duration: 45-60 minutes (including bus, train and walk)*

- **Arriving by train from Luxembourg central station:**

Take the train in direction of "Rodange" or "Petange".
Your stop is called: "Belval-Université" station
*Please note that public transportation is free in Luxemburg!*
Duration: 35 minutes (including train and walk)
A map of the Campus BELVAL and how to find both LCSB buildings is shown below. From Belval train station it's about a 5
minutes-walk to either of our locations.

### 2. Arriving by car:
If you travel by car, please take the highway A4 in direction Esch-sur-Alzette, and stay on that road until it ends at the
roundabout called "Raemerich". Do not leave the A4 too early! Simply follow the A4 until it ends, then follow the signs to
"Belval" at the large roundabout. On the campus, unfortunately, there are no signs leading the way to the LCSB.
Please use the following plan to orientate on the campus. The House of Biomedicine (LCSB I) is directly next to the old blast
furnace site, close to the red Dexia buildings. The building is a bit hidden. It is a six floor modern white building. Biotech II
(LCSB II) is behind the big outdoor parking, right next to ADEM (Agence pour le développement de l'emploi / Employment
Development Agency) and across the street from Southlane Towers.
*Duration: about 20 minutes from Luxembourg City or 30 minutes from Luxembourg Airport (without traffic jam).*

##### Parking at Campus BELVAL
Since Campus Belval is foreseen to be a green car-free Campus, parking may not be easy. Due to the constructions around
our buildings, we currently cannot offer visitor parking. To park your car we recommend using one of the following. From
either site it is less than five minute walk to either of our buildings.
- Parking Square Mile
Price: 0.70€/hour
- Belval Plaza parking garage
Price: first 3 hours free, afterwards 3€/hour (from 07:00 to 20:00) and 1€/hour (from 20:00 to 07:00)

### 3. Arriving by bus:
You can also arrive by bus from Esch-sur-Alzette or from Luxembourg City. Please visit www.mobiliteit.lu to select your
travelling options.
*Please note, however, that if you travel from Luxembourg City, the train is the preferred option.*



![](img/map.jpg)
