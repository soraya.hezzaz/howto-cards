---
layout: page
permalink: /external/general/usefulLinks/
shortcut: general:usefulLinks
redirect_from:
  - /cards/general:usefulLinks
  - /external/cards/general:usefulLinks
---
# Useful links for living in Luxemburg
Here, we compiles a few resources that might facilitate your start in Luxemburg - or also inspire you if you are already living here for a while.
Please note that those are external link and thus the LCSB has no control of the integrity of the content.

- [JustArrived](https://www.justarrived.lu) - a website in both English and French providing you an overview of life, habits and processes in Luxemburgs, also supported with facts and figures.

- [Contrat d’accueil et d’intégration - CIA](https://forum-cai.lu/) - a program from the government designed for new inhabitants of Luxemburg. Information is avaliable in English, French and German.

- [AngloInfo](https://www.angloinfo.com/luxembourg) - a site for expats living in Luxemburg promoting events and activities

- [EURAXESS](https://www.euraxess.lu) - An initiative launched to promote research careers and facilitate the mobility of researchers across Europe