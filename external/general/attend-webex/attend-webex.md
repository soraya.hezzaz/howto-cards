---
layout: page
permalink: /external/general/attend-webex/
shortcut: general:attend-webex
redirect_from:
  - /cards/general:attend-webex
  - /external/cards/general:attend-webex
---
# Attend the LCSB Team Meeting online (webex)

If you want to attend a webex meeting (e.g., the LCSB Team Meeting), download first the Cisco Webex Client from [here](https://unilu.webex.com).

**Note:** On macOS, you might have to grant permission first to run the Cisco Webex Client.

You usually have received a link from the organizer to the meeting. If you click on that link, you will get redirected to a page similar to this one:

  ![](img/image1.png)


You can then login with your university credentials. Once you logged in, a popup will prompt you to join the meeting:

![](img/image2.png)

**Note:** On Windows, you might not be asked for a password.