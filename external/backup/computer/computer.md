---
layout: page
permalink: /external/backup/computer/
shortcut: backup:computer
redirect_from:
  - /cards/backup:computer
  - /external/cards/backup:computer
---
# Staff Computer

## Backup to External Disk

### macOS

macOS platform comes with Time Machine, a very easy to use backup feature. Time Machine backups can be set up as follows:

1. Connect an external disk to your computer.
2. Using Finder open up  **/Applications/System Preferences** and Select **Time Machine**.  <br/>  ![Alt](img/mac_tm_1.png "Title")
3. From the **Time Machine** main screen click **Select Backup Disk**  &nbsp;  ![Alt](img/mac_tm_2.png "Title")
4. From the list select your external disk. It is also recommended that you select the option to **encrypt backups**.   <br/>  ![Alt](img/mac_tm_3.png "Title")
5. If you opt for encryption you will be asked for an encryption password. You will need this password whenever you re-connect the disk to your computer or whenever you restart your Mac. You should keep this password safe using [Password Management software]({{ 'external/access/passwords' | relative_url }}). <br/>  ![Alt](img/mac_tm_4.png "Title")
6. Time Machine requires an apple-specific disk format to work. Your external disk may not be in this format (e.g. may be in exFAT format which is accessible both by Windows and Mac). In such a case Time Machine will prompt you to approve the erasure and re-formatting of your external disk (see below). If you do not want to re-format, you may [create a disk image]({{ 'external/integrity/encryption/file' | relative_url }}) on your external disk and then point Time Machine to this image.  <br/>  ![Alt](img/mac_tm_5.png "Title")
7. After you select a backup disk, provide password and confirm disk format (if applicable), Time Machine will automatically start the backup process in the background. You may continue to work. You will receive a notification, when the first backup is complete.  <br/>  ![Alt](img/mac_tm_6.png "Title")
8. If you want to initiate another backup manually, click the Time Machine logo in your menu bar and select **Back Up Now**. It is recommended that you enable the automatic backup feature of Time Machine. To do so,  go to  **/Applications/System Preferences/Time Machine** and select the option **Back Up Automatically**.   <br/>  ![Alt](img/mac_tm_7.png "Title")

Official instructions for restoring your Mac using a Time Machine backup can be found [here](https://support.apple.com/en-us/HT201250)

### Windows

Windows's offerings for backup has different names in different versions.

* _Microsoft Backup and Restore_, which is available in [Windows 7](http://windows.microsoft.com/en-US/windows7/Back-up-your-files) and [Windows 10](https://support.microsoft.com/en-us/help/17143/windows-10-back-up-your-files), allows backup of your entire system or selected folders/files on your computer.
* _File History_, which is is available in [Windows 8 and 10](http://windows.microsoft.com/en-us/windows-8/set-drive-file-history), provides an easy to use option with which you can backup select files/folders. _File History_ runs periodically, tracks versions of your files, and allows you to go back older versions.

Before creating a backup, make sure that your backup drive is encrypted as a safety measure in case of disk loss or theft (see [howto page on disk encryption]({{ 'external/integrity/encryption/disk' | relative_url }})).

The steps to switch **File History** on your computer is as follows:

1. Connect an external disk to your computer.
2. From the **Start** menu select **Settings/Update & Security/Backup**.  <br/> ![Alt](img/win_fh_1.png "Title")
3. Click the "+" next to Add a drive. You'll see a prompt to choose an external drive, choose the disk you have connected.
4. File History has now started archiving your data. An on/off slider will now appear under a new heading called **Automatically back up my files.** <br/> ![Alt](img/win_fh_2.png "Title")
5. By default, Windows 10's File History will back-up all the folders in the **User** folder. And it will run the backup on an hourly schedule. To change any of those settings click on More options under the on/off slider (see below). <br/> ![Alt](img/win_fh_2_2.png "Title")
6. In the **Backup Options** dialog, under the  **Back up these folders.** you can remove existing folders and add new ones. **Important Note:** Make sure that any application that read/write your data files/folder are closed, when you're making this _File History_ configuration.  <br/>
<img src="img/win_fh_3.png" height="200em"/>


The instructions for restoring your files from **File History** can be found [here](https://it.nmu.edu/docs/backup-using-windows-file-history)


## Backup to dropit.uni.lu

The University of Luxembourg's cloud service [DropIt](https://dropit.uni.lu/) provides all staff with 100GB of storage.

To use this service, first, visit [dropit.uni.lu](https://dropit.uni.lu/). You should be able to login with your university credentials. In case of login problems, request IT support using [ServiceNow](https://service.uni.lu/).

Second, in order to create backups of your computer on DropIt cloud, you need to install a client application. The University IT Guideline [here](https://intranet.uni.lux/the_university/siu/Documents/Dropit%20client%20-%20configuration%20guide.pdf#search=dropit) provides the details on how to install this client application for macOS, Windows and Linux platforms, and also, how to configure the backup process on your computer.
