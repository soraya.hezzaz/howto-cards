---
layout: page
permalink: /external/contribute/web-ide/
shortcut: contribute:web-ide
redirect_from:
  - /cards/contribute:web-ide
  - /external/cards/contribute:web-ide
---

# Contribute using Gitlab Web IDE

This shows how to use Gitlab Web IDE to add or edit a howto-card:

Before you start, please make sure that you have the correct access rights to edit the repository.

*Note: If you do not have the rights to edit the repository, you can either ask for permissions by contacting
the repository maintainers, or use your own copy of the repository (called a `fork`).
Please follow [these instructions](https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html#creating-a-fork) to
obtain your copy (fork). You can then [mirror the fork automatically]({{ '/?contribute:mirror-fork' | relative_url }}) to keep in sync.*

A clear difference between editing a file in a git-tracked repository compared to a live document is that you can make an edit directly, and then suggesting the change. You can make this edit on your own version (called a `branch`), and then request to merge your changes into the main repository (through opening a `merge request`).

## Select the development version (branch)

Before making an edit, please follow the instructions in the `README.md` file. Often, a standard development scheme is followed. For instance, there is a `master` branch (usually referred to as the stable version of the repository) and a `develop` branch (the unstable development version of the repository). There can also be lots of other versions in the same repository. In order to select the development version (`develop` branch), click on the drop-down menu and select `develop`

<img src="img/img1.png">

The page will refresh and show the repository contents. On the top left of the page, you can see that the `develop` branch has been selected.

<img src="img/img2.png">

## Open the web editor (Web IDE)

You can now open the web editor (integrated development environment or IDE) by clicking on the Web IDE button:

<img src="img/img3.png">

Now, you can make changes to the file you wish. As an example, we will make a change to the file `privacy_policy.md`. Select the file `privacy_policy.md` from the list on the left.

<img src="img/img4.png">

Now, edit the file the way you want. For instance. we add a line with `test` in the first section:

<img src="img/img5.png">

*Note: You can see a green line on the left side of it, indicating that a new line has been added. The vertical line turns blue if a line is edited.*

## Committing your change

As already mentioned, a change has to be proposed, and will then be reviewed. The system used is through `merge requests`. Now, as you have edited the file, you can see that on the bottom left of the window, a blue button appeared:

<img src="img/img6.png">

*Note: There is no `Save` button. You save your work by committing changes.*

When clicking on submit, the page changes to display the changes made:

<img src="img/img7.png">

You can see that a green line has been added on the right column. The left column displays the original file (of the `develop` branch).

On the bottom left, you can see a form for committin the change. Here, you have to select `Create a new branch`. This is **important**.

<img src="img/img8.png">

You can also add a specific commit message to label the change properly. The name of the branch is generated automatically. You can, if you want, change it as well.

Then, select `Start a new request` when you are ready to start the submission process and hit the `Commit` button.

## Submitting your merge request

After hitting the `Commit` button, you will be redirect to the submission page for your merge request:

<img src="img/img9.png">

Please make sure that you see that you are proposing to merge your branch (version) into the `develop` branch. This is shown at the top of the page:

<img src="img/img10.png">

You can then fill our the merge request submission form. and select, for instance, an `Assignee`.

Good practice is to select `Delete source branch when merge request is accepted.`

Under `Changes`, you can see the added line:

<img src="img/img11.png">

All that is left now is to submit the merge request by clicking on `Submit merge request`. That is it.

