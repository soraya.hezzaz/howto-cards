---
layout: page
permalink: /external/contribute/vscode/
shortcut: contribute:vscode
redirect_from:
  - /cards/contribute:vscode
  - /external/cards/contribute:vscode
---

# Contribute using Visual Studio Code

This shows how to use Visual Studio Code to write a howto-card:

## Step 0: Update your local copy of your fork

Please update your fork first, by View > Command Palette and searching for 'Git: Fetch from all remotes'.

## Step 1: Create a new Branch

For each how-to card, it is strongly recommended to create a new branch.

- click on:
    <img src="img/visual-code_img_1.png" height="50">  (on the bottom left)

- click on: `create new branch from...`

    <img src="img/visual-code_img_2.png">

- add a name of the branch (e.g., name of the how-to card) and press Enter

- choose `upstream/develop`

    <img src="img/visual-code_img_3.png">

- the new branch is created as can be seen on the bottom left of the screen

    <img src="img/visual-code_img_4.png">


## Step 2: Create a new folder

- To create a new folder, browse to `howto-cards-internal`

    <img src="img/visual-code_img_5.png">

- Then, select the `internal` folder:

    <img src="img/visual-code_img_6.png">

- Click on new folder (the name of the folder should be the name of the howto card). Please only use lowercase letters.

    <img src="img/visual-code_img_7.png" height="60">

## Step 3: Create a new file

- Click on `New file`. Name the file the same as the folder using lowercase letters linked by dashes

    <img src="img/visual-code_img_8.png" height="60">

## Step 4: Write the procedure in Markdown

If you need more details on how to do this, refer to the [corresponding howto-card](/?contribute:markdown).

## Step 5: Commit your changes

- on the left side click on:  <img src="img/visual-code_img_11.png" height="50" width="50">

- below changes click on:

    <img src="img/visual-code_img_12.png">

    stated changes

- add a message (description of your changes) and click "commit"

    <img src="img/visual-code_img_13.png">

## Step 6: Push to the LCSB Gitlab server

- When you push your procedure for the first time click on the cloud icon:

    <img src="img/visual-code_img_15.png">

- Choose origin/"your gitlab user name"
- Open Gitlab: https://gitlab.lcsb.uni.lu/

    <img src="img/visual-code_img_16.png">

- To synchronize changes: <img src="img/visual-code_img_17.png">
