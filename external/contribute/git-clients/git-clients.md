---
layout: page
permalink: /external/contribute/git-clients/
shortcut: contribute:git-clients
redirect_from:
  - /cards/contribute:git-clients
  - /external/cards/contribute:git-clients
---
# Git clients

Installation instructions are provided [here]({{ '/?contribute:install-git' | relative_url }}).

## GUI clients

Here is a list of suggested Git GUI clients:

* [Sourcetree](https://www.sourcetreeapp.com)
* [Fork](https://git-fork.com)
* [SmartGit](https://www.syntevo.com/smartgit/)

You can find more clients on the [Git webpage](https://git-scm.com/downloads/guis).

## IDEs

There are also several IDEs (integrated development environments) that have git functionalities included:

* [GitLab WebIDE](https://docs.gitlab.com/ee/user/project/web_ide/)
* [Visual Studio Code](https://code.visualstudio.com/)
* [Atom](https://atom.io/)
* [IntelliJ](https://www.jetbrains.com/idea/)
* [PyCharm](https://www.jetbrains.com/pycharm/)