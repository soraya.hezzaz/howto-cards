---
layout: page
permalink: /external/contribute/install-git/
shortcut: contribute:install-git
redirect_from:
  - /cards/contribute:install-git
  - /external/cards/contribute:install-git
---

# Installation of Git

Several git clients are presented [here]({{ '/?contribute:git-clients' | relative_url }}).

For most of the git clients, `git` is actually required to be installed on the system. You can find installers for Windows, MacOS and Linux on the [Git webpage](https://git-scm.com/downloads).

If you are using Visual Studio Code, and if you want to contribute to a repository hosted on the [LCSB Gitlab](https://gitlab.lcsb.uni.lu), you will need to verify the host key first before you are able to clone or push to a repository. For this, open a terminal (on Linux or macOS) or Git Bash (on Windows), and type:
```bash
ssh git@gitlab.lcsb.uni.lu -p8022
```
Then, press Enter and type `yes`. This will add the Gitlab server to the known hosts on your system.