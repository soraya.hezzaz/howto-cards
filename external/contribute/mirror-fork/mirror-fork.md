---
layout: page
permalink: /external/contribute/mirror-fork/
shortcut: contribute:mirror-fork
redirect_from:
  - /cards/contribute:mirror-fork
  - /external/cards/contribute:mirror-fork
---
# Mirror fork automatically

In order to keep your fork up-to-date automatically with the main (also called `upstream`) repository,
you should follow the follow simple steps.

1. Go to `Settings > Repository` (bottom left of the screen)
2. Expand the section on `Mirroring repositories`
3. In the field `Git repository URL`, enter the SSH clone address from the main repository. In case of this repository, the address is `ssh://git@gitlab.lcsb.uni.lu:8022/R3/howto-cards.git`
4. Select `Mirror Direction` as `Pull`
5. Click on `Detect Host Keys`
6. `Authentication method` should be selected as `SSH public key`
7. Then, click on the blue button `Mirror repository`

You will see an entry in the table below the blue button. Often, there is an error appearing. Now, in order to be able to perform the mirror, you need to set the SSH key.

1. Copy the SSH public key by clicking on the copy button (next to the sync button) located next to the red paper bin.

    <img src="img/copy-ssh-key.png" height="80">

2. Then, browse to your profile picture (top right) and click on `Preferences`
3. On the left of the page, click on `SSH keys`
4. Paste the key (using CTRL+V or CMD+V) into the SSH field
5. Click on `Add key`

Now, the synchronization of the fork should perform successfully. You can click on the sync button or wait a few minutes. :white_check_mark:
