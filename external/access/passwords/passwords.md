---
layout: page
permalink: /external/access/passwords/
shortcut: access:passwords
redirect_from:
  - /cards/access:passwords
  - /external/cards/access:passwords
---
# Managing your passwords

 There are several tools that can be used for password management. Some provide limited (basic) features for free, some require a fee. Below are some password management tools that can be used:


|Tool|Notes|
|-------------------------|------------------------|
| [LastPass](https://www.lastpass.com)| Free for most features. Paid access allows secure sharing of passwords e.g. for shared infrastructure. If you're in charge of the archival and management of research data for your group, please check with the LCSB IT Team whether they can provide you a LastPass subscription.|
| [dashlane](https://www.dashlane.com) |Free for limited features. |
| [KeePass](https://keepass.info)| Free.|



# Securely sending passwords to others

When sending passwords to collaborators, it is not advised to use e-mail or instant messaging. The preferred way is to generate **password links**, and then send these links to collaborators. Password links can have varying expiry periods; a link may expire once visited, or it be valid for a certain duration. Below are a list of freely available tools that can be used to generate password links. We recommend that you use  **Privatebin hosted by the LCSB**.


|Tool|Notes|
|-----|------------------------|
| [Privatebin @ LCSB](https://privatebin.lcsb.uni.lu) (Recommended)|Free service provided by the LCSB. LUMS account required. |
| [Privatebin @ Uni-LU HPC](https://hpc.uni.lu/privatebin/) |Free service provided by the HPC team of Uni LU. No signup required.|
| [Zerobin](https://zerobin.net/) | Free. No signup required.|
| [Noteshred](https://www.noteshred.com) | Free. Signup required.|


## Privatebin hosted by the LCSB

### Sending a password
  1. 1. Go to [Privatebin @ LCSB](https://privatebin.lcsb.uni.lu/) and type the password you want to share into the Editor tab. You will be asked to enter your LUMS credentials when you click on "Send"<br/>
  <img src="img/privatebin-editor.png">

  1. Set expiry period
    -  For the highest safety measures, sender can allow password link to be used only once so it expires upon first access. This feature can be set by checking **Burn after reading** checkbox.

  1. You can also enter a password to protect your password link - this should be **different** from the password you are sharing!
  1. Click **Send** in right-top corner
    - you should be redirected to a page containing the password and password link
    <img src="img/privatebin-link.png"><br/>
  1. Share the **link** with your collaborator via your favorite communication channel

### Receiving a password
  * You have been provided a link to website where your new password is stored. (This link can be protected by yet another password you might be asked to enter.)
<img src="img/privatebin-password.png"><br/>

  * If you see following:
<img src="img/privatebin-expired.png"><br/>
    * You have either entered the wrong/incomplete link - try copy-pasting the link into your browser instead of clicking on it - or
    * your password link has already expired. In that case you will have to request a new link.<br/>
      **Warning!**: If the password link was set with one access only (**Burn after reading**) and you are sure that you haven't accessed the link so far, you should notify the password sender because there is a chance that your communication was intercepted and someone accessed the link before you.<br/>
      **Note**: If the page looks different from the pictures above, try using Google Chrome browser. If the issue persists, please take a screenshot of the page and send it to our [sysadmin](mailto:lcsb-sysadmins@uni.lu) team.

# Tips when resetting University of Luxembourg Active Directory password on a Mac

To reset your UL AD password when using a Mac:

 1. Go to <https://owa.uni.lu> and change your password there.
 2. **Before** rebooting your Mac, you should change your keychain password to avoid any issues down the road. To do so, run the following command `security set-keychain-password "/Users/YOURHOME/Library/Keychains/login.keychain-db"`
 3. If you use FileVault, after the first reboot, first enter your old password, then the new one. Normally, FileVault will sync and update with your new password after you reboot.
