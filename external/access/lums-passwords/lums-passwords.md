---
layout: page
permalink: /external/access/lums-passwords/
shortcut: access:lums-passwords
redirect_from:
  - /cards/access:lums-passwords
  - /external/cards/access:lums-passwords
---
# LUMS account

In addition to the standard university account, LCSB researchers and external collaborators are provided with the LUMS (LCSB User Management System) account.
This serves for internal authentication for resources provided by the LCSB, e.g. LCSB hosted [GitLab](https://gitlab.lcsb.uni.lu), LCSB [OwnCloud](https://owncloud.lcsb.uni.lu) storage or access to virtual machines.

## Requesting LUMS account

  1. Go to [service.uni.lu](https://service.uni.lu).

  2. Login with your university credentials.

  3. Create a ticket requesting a LUMS account:

     *  Choose **LCSB** ticket service:<br/>
<img src="img/lums_ticket-main.png" height="300px"><br/>

     *  Navigate to **"BioCore: Application Services"** and choose **"[Request for] a new LUMS account**:<br/>
    <img src="img/lums_ticket-request.png" height="300px"><br/>

     * If you know already to which services you need access, please specify it in the ticket.

In case you are not a member of the university, please ask your collaborator at the university to request the account for you.

## (Re-)Activating LUMS account upon receiving credentials or after password reset

The password you will receive from the system administrators is temporary and **it will expire after one month**, so you have to reset it as soon as possible. It is usually sent as a link to [PrivateBin]({{ '/?access:passwords' | relative_url }}). **This link is valid for one week and is deleted after you see it once**, thus you need to keep it open until you have followed these steps to reset your password:

  1. Go to [lums.uni.lu](https://lums.uni.lu) and login with your LUMS username and the temporary password you received.<br>**NOTE:** If you experience any issues at this step, try to use *Chrome* browser for this procedure.<br/><img src="img/lums_login.png" height="200px"><br/>
  Should the login look different from this, e.g. like a pop-up, click on **x** or **Cancel** to close it.

  2. You will be prompted to reset your password:<br/>
  <img src="img/lums_first-reset-password.png" height="300px"><br/>
  3. Enter your temporary password into the field **"Current Password"**.
  4. Leave the **"OTP"** field blank.
  5. Enter a new password of your choice in the field **"New Password"** and repeat the same password in the field **"Verify Password"**. This is the actual password you need to use from now on, so make sure to remember it.
  6. Click on **"Reset Password and Login"**.

It is a good practice to use a password manager which will not only help you to keep track of your passwords so you will never forget it, but it will also help you to generate passwords which are very safe (see [HowTo card]({{ '/?access:passwords' | relative_url }}) on password management for more detail).

## Changing password for LUMS accounts
If you want to change your password for the LUMS account, you can:
  1. Go to [lums.uni.lu](https://lums.uni.lu) and login with your LUMS credentials:
  <br/><img src="img/lums_login.png" height="200px">

  2. In the top-right corner click on your name and select **Change password**:
  <br/><img src="img/lums_home-settings.png" height="300px">

  3. Enter your current password and your new password:
  <br/><img src="img/lums_reset-password.png" height="300px">
