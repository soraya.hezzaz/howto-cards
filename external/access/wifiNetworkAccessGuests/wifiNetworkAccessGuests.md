---
layout: page
permalink: /external/access/wifiNetworkAccessGuests/
shortcut: access:wifiNetworkAccessGuests
redirect_from:
  - /cards/access:wifiNetworkAccessGuests
  - /external/cards/access:wifiNetworkAccessGuests
---
# WiFi network access for guests

- Each UL staff member can provide wireless network access for her/his visitors by creating an account on the [Guest Sponsor Portal](https://sponsor.uni.lux:8443/sponsorportal/PortalSetup.action?portal=06f49c40-ad3a-11e4-ba4b-0050568059d7)
- In order to create the guest account, you must log in with your normal UL username and password
- Please also refer to the [process for visitor rules](https://howto.lcsb.uni.lu/?hr:visitorsRules)
- Such a visitor account has a lifetime of maximum 5 days. If you need accounts for people staying longer, please open a [ticket via the SIU helpdesk](https://service.uni.lu/sp)
- Guests will only have access to the internet and public services but not to internal resources

Other documentations on how to use the SIU systems can be found on the [UL intranet (section SIU-SIU-Guides-Wireless Network)](https://intranet.uni.lux/the_university/siu/Pages/siu-guides.aspx).

For any questions please open a [ticket via the SIU helpdesk](https://service.uni.lu/sp).

<!--Process owner: Julia Kessler-->

