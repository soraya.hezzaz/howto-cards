---
layout: page
permalink: /external/access/harrenhal-access/
shortcut: access:harrenhal-access
redirect_from:
  - /cards/access:harrenhal-access
  - /external/cards/access:harrenhal-access
---
# HARRENHAL access

HARRENHAL is a gateway to provide our external collaborators access to servers that are hosted at the LCSB via a web browser.

## How to access HARRENHAL website ?

Launch your favorite web browser and go to [HARRENHAL](https://harrenhal.uni.lu)

## First time on HARRENHAL ?

1. Enter your credentials

   * Username - firstname.lastname
   * Password - Temporary password provided by LCSB's sysadmins.
   * Click **Login**

   <img src="img/login_01.png" height="450px"><br/>

2. Reset your temporary password

   * New Password - New password that you want to use.
   * Confirm New Password - Confirm the new password.
   * Click **Continue**

   <img src="img/login_02.png" height="300px"><br/>

3. Configure the two-factor authentication.

   * Scan the QR code with your favorite two-factor authentication app. Or click **show** to display the TOTP key.
   * Enter the 6-digit authentication code provided by your two-factor authentication app.
   * Click **Continue**

   <img src="img/login_03.png" height="650px"><br/>

**Note:** Do not forget to backup your two-factor authentication app account or the TOTP key.

## How to login to your HARRENHAL account ?

Once you successfully complete the password reset and the two-factor authentication enrollment processes, you can login now to your HARRENHAL account.

1. Enter your credentials

   * Username - firstname.lastname
   * Password - Password you have set.
   * Click **Login**

   <img src="img/login_01.png" height="450px"><br/>

2. Enter your two-factor authentication code

   * Authentication Code - 6-digit authentication code provided by your two-factor authentication app.
   * Click **Continue**

   <img src="img/login_04.png" height="350px"><br/>

## How to access a VM ?

1. On your homepage under **ALL CONNECTIONS** section, you can view all the VMs that you are allowed to connect to by clicking on the drop list button.

   <img src="img/all_connections_01.png" height="150px"><br/>

2. Access to a VM in the list by clicking on it.

   <img src="img/server_01.png" height="500px"><br/>

## How to change your password ?

Go to your account menu and click **Settings**

   * Current Password - Current password you want to change.
   * New Password - New password you want to use.
   * Confirm New Password - Same new password.
   * Click **Update Password**

   <img src="img/change_password_01.png" height="200px"><br/>
