---
layout: page
permalink: /external/publication/10WaysImproveEnglish/
shortcut: publication:10WaysImproveEnglish
redirect_from:
  - /cards/publication:10WaysImproveEnglish
  - /external/cards/publication:10WaysImproveEnglish
---
# 10 ways to improve your English

Francisco ARAGAON ARTACHO, former postdoc in the Systems Control group, has made a list of the ten most important points that helped to improve his English. In this article he shares them with you and explains what he means by each of them.

**1. Believe improving your English is something very important**
I think this is the most important point. If you don't believe a good written English is important, you will never improve it. So... why is it important? The way you write will give your reader a first impression about you. Even more, the reader will even get an impression about your personality (tidy / messy / arrogant / smart / knowledgeable / dumb). He / she probably doesn't know you, nor knows how good your ideas are. Don't give him / her a bad first impression: it will be hard to change his/her mind. While refereeing, I've directly rejected several papers because the English was terribly bad, or because it was full of typos. Nobody wants to read a text with many typos and which is clearly against the "golden rule" to never make a referee angry! Before submitting a paper / grant proposal, check it carefully. If one of your co-authors is a native English speaker, ask him / her to check the grammar of what you wrote. If not, ask a friend. If possible, wait for a week and read it again. You will probably discover that what you wrote is not as nice as you thought. You will find many typos and errors.

**2. Read**
Read as much texts in English as you can, especially novels. If you read good writers, perhaps (probably) something good will stick in your mind. Try to use their "fancy words and sentences" whenever it is possible.

**3. Listen**
I believe listening to music and watching movies / TV shows helped a lot to improve my English. Even during the most boring talk at a conference you may learn something good for your English.

**4. Use synonyms**
Synonyms are good. Avoid repeating words / structures whenever is possible. Use as many different sentence connectors as you can / know. Your text will look better and will be nicer to read. I normally use [Thesaurus](http://thesaurus.reference.com/).

**5. Use a dictionary**
While writing, I always have a dictionary open in my browser. I especially like [wordreference.com](http://www.wordreference.com/), which even has the British / American pronunciation for many words.

**6. Check what Google says about your sentences**
If you're not sure about a sentence you wrote, google it (or part of it), using quotation marks. If there are only a few hits, your sentence is probably wrong. If you doubt between two versions of the same sentence, the one with more results in Google is probably going to be the correct one.

**7. Write short sentences**
Long sentences make the reader feel exhausted, so do your best to avoid them. Try to punctuate correctly: use commas, colons, and semicolons.

**8. Write short paragraphs**
In a similar way to the previous point, long paragraphs are exhausting. I remember that when I was a kid I hated books without "white space" (breaks), and I guess I still have some similar feeling. A text with short paragraphs always looks easier to read at first glance. Additionally, while aiming to write short paragraphs, you will be forced to structure your ideas (next point).

**9. Structure your thoughts through the text**
Structure your ideas to explain yourself. It is very important to use sentence connectors, as for example: moreover, however, besides, although, nevertheless, in spite of, then, therefore, thus, hence, whence, on one hand / on the other hand, additionally, further, furthermore, otherwise...

**10. Ask people to correct you**
People (wrongly) believe that it is impolite to correct others when they're not speaking in their native language. I guess this happens because no one likes to be wrong, and even less to be pointed out for being wrong. At the beginning, when I started learning English, people used to correct me very often. Unfortunately, as soon as my English was "good enough", I noticed that I was getting less and less corrections, despite my English wasn't that good. For example, only last year my Aussie housemate told me that I was pronouncing "yellow" wrong (and, probably, every single word starting with "y" - pronouncing it like "j" in "jelly") because he wanted to piss me off. At that moment I guess he was disappointed to see that I liked to be corrected, but he started telling me my mistakes since then. Thus, if you want to be corrected you'll need to ask the others to do it, and remember to thank them with a not-very-fake-looking smile when they do it :).

