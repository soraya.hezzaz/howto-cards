---
layout: page
permalink: /external/ppc/publish-repo/
shortcut: ppc:publish-repo
redirect_from:
  - /cards/ppc:publish-repo
  - /external/cards/ppc:publish-repo
---

# Publish a repository

By default, the repositories created on the LCSB Gitlab are private, i.e. they cannot be seen by anyone outside LCSB.

In order to change the visibility of the repository, you can do so by browsing to `Settings` and then expanding the section on `Visibility, project features, permissions`.

There, you can set the visibility to `Public`. It is important to note that the group must be public in order to change visibility of the repository.

Remember to follow the [LCSB policy regarding code development](https://howto.lcsb.uni.lu/?policies:LCSB-POL-BIC-07). In case of questions, please do not hesitate to contact the [R3 team](mailto:lcsb-r3@uni.lu).