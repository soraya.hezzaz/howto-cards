---
layout: page
permalink: /external/ppc/add-gitignore/
shortcut: ppc:add-gitignore
redirect_from:
  - /cards/ppc:add-gitignore
  - /external/cards/ppc:add-gitignore
---

# Add a .gitignore to your repository

The version control system git tracks all files in a repository.

Sometimes, you do not want certain files or directories to be tracked, but fully ignored by `git`. These files or directories
can include binaries, images, dependencies, log files, or analysis results. Also, hidden system files are usually picked up by `git`.

The idea is to create a file named `.gitignore` that lists all the files and directories that should be ignored by `git`.

For instance, in order to ignore all log files with the suffix `.log`, the content of the `.gitignore` file should be:

```
*.log
```

A common example is to ignore system files as well, such as the `.DS_Store` file:

```
.DS_Store
```

*Note:* Sometimes, you might not see the hidden system files. You can do so by browsing to the directory in the terminal and typing:

```bash
$ ls -lash
```

Once the `.gitignore` file has been created, you can add and commit the file as any other file in the `git`-tracked repository. A relatively complete collection of `.gitignore` templates can be found [here](https://github.com/github/gitignore).

More information on the `.gitignore` mechanism can be found [here](https://www.atlassian.com/git/tutorials/saving-changes/gitignore).
