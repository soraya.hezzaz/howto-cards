

stages:
  - prepare
  - save
  - build
  - check
  - generate
  - deploy
  - trigger

variables:
    GIT_STRATEGY: clone
    DOCKER_DRIVER: overlay2
    DOCKER_TLS_CERTDIR: ""

# prepare
# ------------------------------------------------------------------------------------

prepare:index:
  image: python
  stage: prepare
  rules:
    - if: $CI_COMMIT_REF_NAME
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event" && $CI_COMMIT_MESSAGE !~ /tmpBranch/ && $CI_COMMIT_MESSAGE !~ /Update index/'
  before_script:
    - pip install natsort
  script:
    - python .ci/generateIndex.py
    - mkdir .tmp
    - cp cards.md .tmp/.
    - cp .ci/whitelist.txt .tmp/.
  artifacts:
    expire_in: 1 day
    paths:
      - .tmp

# save
# ------------------------------------------------------------------------------------

save:commitIndex:
  image:
    name: alpine/git:latest
    entrypoint: [""]
  stage: save
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event" && $CI_COMMIT_MESSAGE !~ /tmpBranch/ && $CI_COMMIT_MESSAGE !~ /Update index/ && $CI_MERGE_REQUEST_SOURCE_PROJECT_PATH == "R3/howto-cards"'
  before_script:
    - apk add git-lfs
    - git fetch --all
    - url_host=`git remote get-url origin | sed -e "s/https:\/\/gitlab-ci-token:.*@//g"`
    - git remote set-url origin "https://$GIT_ACCESS_USER:$GIT_ACCESS_TOKEN@${url_host}"
    - git config user.name $GIT_ACCESS_USER
    - git config user.email $GIT_ACCESS_EMAIL
  script:
    # copy the index and remove the temporary directory
    - cp .tmp/cards.md .
    - rm -rf .tmp

    # commit
    - git branch -D tmpBranch || true
    - git checkout -b tmpBranch || true
    - git add --all || true
    - git commit -m "Update index" || true
    - git checkout $CI_COMMIT_REF_NAME || true
    - git pull origin $CI_COMMIT_REF_NAME
    - git merge tmpBranch --ff || true
    - git push origin $CI_COMMIT_REF_NAME || true
    - git branch -D tmpBranch || true

# build
# ------------------------------------------------------------------------------------

build:pages:
  image: $CI_REGISTRY/r3/docker/jekyll-lcsb:1.8
  stage: build
  variables:
    JEKYLL_ENV: production
    BUNDLER_VERSION: 2.0.2
  artifacts:
    expire_in: 1 day
    paths:
      - build
      - .tmp
  rules:
    - if: $CI_COMMIT_REF_NAME
  before_script:
    - apt-get -qq update
    - apt-get install -y -qq git-lfs
    - gem install bundler:$BUNDLER_VERSION && bundle install
  script:
    # Generate the configuration for forks (will use Gitlab Pages on personal namespaces)
    - |
      if [ $CI_MERGE_REQUEST_ID ]; then
          export CI_COMMIT_REF_NAME="develop";
      else
          echo "url: https://$CI_PROJECT_NAMESPACE.$CI_PAGES_DOMAIN" >> .ci/_config_gitlab_pages.yml;
          echo "baseurl: /$CI_PROJECT_NAME" >> .ci/_config_gitlab_pages.yml;
          # If there is no config for the current branch, use the one for Gitlab Pages
          if [ ! -f ".ci/_config_$CI_COMMIT_REF_NAME.yml" ]; then
              mv .ci/_config_gitlab_pages.yml .ci/_config_$CI_COMMIT_REF_NAME.yml;
          fi
      fi
    # Display, which configuration is used
    - |
      echo "Configuration: " && cat ".ci/_config_$CI_COMMIT_REF_NAME.yml"
    # Run Jekyll with custom configuration
    - bundle exec jekyll build -d build --config "_config.yml,.ci/_config_$CI_COMMIT_REF_NAME.yml"

# check
# ------------------------------------------------------------------------------------

check:links:
   stage: check
   image: $CI_REGISTRY/r3/apps/tailorbird/linkchecker
   rules:
     - if: $CI_COMMIT_REF_NAME
     - if: $CI_MERGE_REQUEST_ID
   allow_failure: true
   before_script:
     - cp .tmp/whitelist.txt build/.
     - cp -r build /check
     - cd /
   script:
     - python link_check.py

# generate
# ------------------------------------------------------------------------------------

generate:searchIndex:
  stage: generate
  image: node:13.8.0-slim
  variables:
    LUNR_VERSION: 2.3.8
    DIRECTORY: build
  script:
    - bash .ci/build_indices.sh
    - mv build processed_build
  artifacts:
    expire_in: 1 day
    paths:
      - processed_build

# deploy
# ------------------------------------------------------------------------------------

# Deploy to Gitlab Pages - to be run only by forks
pages:
  image: alpine:3.11.3
  stage: deploy
  rules:
    - if: '$CI_PIPELINE_SOURCE != "merge_request_event" && $CI_COMMIT_REF_NAME == "develop" && $CI_PROJECT_PATH == "R3/howto-cards"'
    - if: '$CI_PIPELINE_SOURCE != "merge_request_event" && $CI_COMMIT_REF_NAME == "master" && $CI_PROJECT_PATH == "R3/howto-cards"'
  script:
    - mv processed_build public
  artifacts:
    expire_in: 1 week
    paths:
      - public

## Deploys the website to a separate VM
deploy:vm:
  stage: deploy
  image: alpine:3.1
  before_script:
    - 'which ssh-agent || ( apk add --update openssh )'
    - eval $(ssh-agent -s)
    - echo "$B64SSHPRIVKEY" | base64 -d | tr -d '\r' | ssh-add - > /dev/null
    - mkdir -p ~/.ssh && chmod 700 ~/.ssh
    - 'echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config'
    - echo "$KNOWNHOSTS" > ~/.ssh/known_hosts
    - chmod 644 ~/.ssh/known_hosts
  rules:
    - if: '$CI_PIPELINE_SOURCE != "merge_request_event" && $CI_COMMIT_REF_NAME == "develop" && $CI_PROJECT_PATH == "R3/howto-cards"'
    - if: '$CI_PIPELINE_SOURCE != "merge_request_event" && $CI_COMMIT_REF_NAME == "master" && $CI_PROJECT_PATH == "R3/howto-cards"'
  script:
    - ssh -p $SSHPORT $SSHCONNECT "mkdir -p ~/$CI_COMMIT_REF_NAME/sources/public/$CI_JOB_ID ~/$CI_COMMIT_REF_NAME/public"
    - scp -P $SSHPORT -r processed_build/* $SSHCONNECT:~/$CI_COMMIT_REF_NAME/sources/public/$CI_JOB_ID
    - ssh -p $SSHPORT $SSHCONNECT "cd ~/$CI_COMMIT_REF_NAME/public && ln -fs ../sources/public/$CI_JOB_ID/* . && cd ~/$CI_COMMIT_REF_NAME/sources/public/ && find . -type d -not -newermt '-1 minutes' -exec rm -rf {} +;"

# Trigger pipelines in internal repo
# ------------------------------------------------------------------------------------

trigger:
  stage: trigger
  image: curlimages/curl
  rules:
    - if: '$CI_COMMIT_BRANCH == "master" || $CI_COMMIT_BRANCH == "develop"'
  tags:
    - privileged
  script:
    - curl --silent --output /dev/null -X POST -F token=$INTERNAL_TRIGGER_TOKEN -F ref=$CI_COMMIT_BRANCH $INTERNAL_REPO
