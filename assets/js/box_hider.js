/**
 * This is used to hide the sections of the index page
 * based on the URL fragment (a.k.a. "hash")
 */
window.boxHider = (function() {
    function GetSelectedId() {
        var boxId = window.location.hash + '-card';
        if (boxId.length > 0) {
            return boxId.substring(1);
        }
        return "";
    }

    function GetAllBoxElements() {
        return document.getElementsByClassName('index-box');
    }

    function GetAllBoxElementsArray() {
        var allBoxes = GetAllBoxElements();
        var allBoxesArray = Array.prototype.slice.call(allBoxes);
        return allBoxesArray;
    }

    function GetSelectedBoxElement(id) {
        var allBoxesArray = GetAllBoxElementsArray();
        var element = document.getElementById(id);

        if (allBoxesArray.includes(element)) {
            return element;
        }

        return false;
    }

    function HideElement(element) {
        if (element instanceof HTMLElement) {
            element.style['display'] = 'none';
        }
    }

    function ShowElement(element) {
        if (element instanceof HTMLElement) {
            element.style['display'] = 'inline-block';
        }
    }

    function HideAllCategories() {
        var allCategoriesElement = document.getElementById('all-categories');
        HideElement(allCategoriesElement);
    }

    function ShowAllBoxes() {
        var allBoxes = GetAllBoxElementsArray();
        allBoxes.map(function(box) {
            ShowElement(box);
        });
    }

    function Trigger() {
        // First, try to get the hash from the URL (https://example.com/uri?param#hash)
        var boxId = GetSelectedId();
        if (boxId.length == 0) {
            // If there is no hash in the URL, just show all the boxes
            ShowAllBoxes();
            return;
        }
        // Otherwise, proceed to getting the corresponding div element
        var selectedBox = GetSelectedBoxElement(boxId);
        if (selectedBox == false) {
            // If the user selection is not a `div.index-box`, then just show all the boxes
            ShowAllBoxes();
            return;
        }
        // Hide the "All Categories" element
        HideAllCategories();
        // Finally, hide all boxes except of the selected one
        // Moreover, make sure that the selected boxes are displayed.
        var allBoxes = GetAllBoxElementsArray();
        if (boxId.startsWith('handbook')) {
            allBoxes.map(function(box) {
                if (!box.id.startsWith('handbook')) {
                    HideElement(box);
                } else {
                    ShowElement(box);
                }
            });
        } else {
            allBoxes.map(function(box) {
                if (box != selectedBox) {
                    HideElement(box);
                } else {
                    ShowElement(box);
                }
            });
        }
    }

    return {
        'Trigger': Trigger,
        'ShowAllBoxes': ShowAllBoxes
    }
})();
