---
layout: default
title: Cards
order: -1
---

{% include scripts.html %}

<style>
.noborderbox {
    border :0;
}
</style>

{% comment %}
	This code adds CSS that hides the index-boxes by default.
	It's in javascript, so that it doesn't effect the browsers with javascript disabled.
	How the correct boxes are shown then? box_hider.js shows them
{% endcomment %}
<script>
	var element = document.createElement('style');
	var content = document.createTextNode(".index-box {display: none;}");
	var head = document.getElementsByTagName('head');
	if (head.length > 0) {
		element.appendChild(content);
		head[0].appendChild(element);	
	}
</script>

<!-- index -->

<div class="index-box-container">

	<div class="index-box noborderbox" id="access-card">
		<h3>Access</h3>
		<ul>
			<li><a href="{{ 'external/access/harrenhal-access' | relative_url }}">HARRENHAL access</a></li>
			<li><a href="{{ 'external/access/lums-passwords' | relative_url }}">LUMS account</a></li>
			<li><a href="{{ 'external/access/passwords' | relative_url }}">Managing your passwords</a></li>
			<li><a href="{{ 'external/access/vpn-cerbere-access' | relative_url }}">VPN/CERBERE connection</a></li>
			<li><a href="{{ 'external/access/wifiNetworkAccessGuests' | relative_url }}">WiFi network access for guests</a></li>

		</ul>
	</div>
	<div class="index-box noborderbox" id="backup-card">
		<h3>Backup</h3>
		<ul>
			<li><a href="{{ 'external/backup/computer' | relative_url }}">Staff Computer</a></li>

		</ul>
	</div>
	<div class="index-box noborderbox" id="contribute-card">
		<h3>Contribute</h3>
		<ul>
			<li><a href="{{ 'external/contribute/git-clients' | relative_url }}">Git clients</a></li>
			<li><a href="{{ 'external/contribute/install-git' | relative_url }}">Installation of Git</a></li>
			<li><a href="{{ 'external/contribute/markdown' | relative_url }}">Markdown</a></li>
			<li><a href="{{ 'external/contribute/mirror-fork' | relative_url }}">Mirror fork automatically</a></li>
			<li><a href="{{ 'external/contribute/vscode' | relative_url }}">Contribute using Visual Studio Code</a></li>
			<li><a href="{{ 'external/contribute/web-ide' | relative_url }}">Contribute using Gitlab Web IDE</a></li>

		</ul>
	</div>
	<div class="index-box noborderbox" id="exchange-channels-card">
		<h3>Exchange channels</h3>
		<ul>
			<li><a href="{{ 'external/exchange-channels/asperaweb' | relative_url }}">AsperaWEB Quick Guide</a></li>
			<li><a href="{{ 'external/exchange-channels/calendar' | relative_url }}">Sharing calendar in Microsoft Exchange</a></li>
			<li><a href="{{ 'external/exchange-channels/owncloud' | relative_url }}">Owncloud</a></li>

		</ul>
	</div>
	<div class="index-box noborderbox" id="general-card">
		<h3>General</h3>
		<ul>
			<li><a href="{{ 'external/general/BelvalCampusMap' | relative_url }}">Belval Campus Map</a></li>
			<li><a href="{{ 'external/general/attend-webex' | relative_url }}">Attend the LCSB Team Meeting online (webex)</a></li>
			<li><a href="{{ 'external/general/getToLCSB' | relative_url }}">How to get to the Luxembourg Centre for Systems Biomedicine</a></li>
			<li><a href="{{ 'external/general/glossary' | relative_url }}">Glossary</a></li>
			<li><a href="{{ 'external/general/links' | relative_url }}">Important websites</a></li>
			<li><a href="{{ 'external/general/remote-working' | relative_url }}">Work remotely</a></li>
			<li><a href="{{ 'external/general/usefulLinks' | relative_url }}">Useful links for living in Luxemburg</a></li>

		</ul>
	</div>
	<div class="index-box noborderbox" id="integrity-card">
		<h3>Integrity</h3>
		<ul>
			<li><a href="{{ 'external/integrity/checksum' | relative_url }}">Ensuring Integrity of Data Files with Checksums</a></li>
			<li><a href="{{ 'external/integrity/encryption/cloud' | relative_url }}">Data upload to cloud</a></li>
			<li><a href="{{ 'external/integrity/encryption/disk' | relative_url }}">Encrypting the Startup Disk for Your Laptop/Desktop</a></li>
			<li><a href="{{ 'external/integrity/encryption/file' | relative_url }}">Encrypting Files and Folders</a></li>
			<li><a href="{{ 'external/integrity/naming' | relative_url }}">Naming files</a></li>
			<li><a href="{{ 'external/integrity/organization' | relative_url }}">Organization</a></li>
			<li><a href="{{ 'external/integrity/sanitisation' | relative_url }}">Sanitising Data Files</a></li>
			<li><a href="{{ 'external/integrity/spreadsheets' | relative_url }}">Working with spreadsheets</a></li>

		</ul>
	</div>
	<div class="index-box noborderbox" id="lab-card">
		<h3>Lab</h3>
		<ul>
			<li><a href="{{ 'external/lab/book-lab-equipment' | relative_url }}">How to book a Lab Equipment in Quarks</a></li>
			<li><a href="{{ 'external/lab/dishwasher-utilization-and-maintenance' | relative_url }}">Dishwasher utilization and maintenance</a></li>
			<li><a href="{{ 'external/lab/handwashing' | relative_url }}">Handwashing</a></li>
			<li><a href="{{ 'external/lab/lab-coats' | relative_url }}">Lab coats</a></li>
			<li><a href="{{ 'external/lab/maintenance_of_cold_traps' | relative_url }}">Maintenance of cold traps  </a></li>
			<li><a href="{{ 'external/lab/personal-alert-safety-system' | relative_url }}">Personal alert safety system (PASS)</a></li>
			<li><a href="{{ 'external/lab/quarks-general' | relative_url }}">Quarks - General information</a></li>
			<li><a href="{{ 'external/lab/utilization-of-balances' | relative_url }}">Utilization of balances</a></li>
			<li><a href="{{ 'external/lab/utilization-of-pH-meter' | relative_url }}">Utilization of pH meter</a></li>

		</ul>
	</div>
	<div class="index-box noborderbox" id="on-offboarding-card">
		<h3>On offboarding</h3>
		<ul>
			<li><a href="{{ 'external/on-offboarding/checklistArrival' | relative_url }}">Checklist upon arrival in Luxemburg</a></li>
			<li><a href="{{ 'external/on-offboarding/checklistBeforeArriving' | relative_url }}">Newcomer Checklist before Arriving in Luxembourg</a></li>
			<li><a href="{{ 'external/on-offboarding/checklistGetStarted' | relative_url }}">Newcomer's checklist</a></li>
			<li><a href="{{ 'external/on-offboarding/checklistGodparent' | relative_url }}">Checklist for godparents</a></li>
			<li><a href="{{ 'external/on-offboarding/godparent' | relative_url }}">Godparents for Newcomers</a></li>
			<li><a href="{{ 'external/on-offboarding/onboarding' | relative_url }}">Onboarding new members at the LCSB</a></li>

		</ul>
	</div>
	<div class="index-box noborderbox" id="ppc-card">
		<h3>PPC</h3>
		<ul>
			<li><a href="{{ 'external/ppc/add-gitignore' | relative_url }}">Add a .gitignore to your repository</a></li>
			<li><a href="{{ 'external/ppc/publish-repo' | relative_url }}">Publish a repository</a></li>

		</ul>
	</div>
	<div class="index-box noborderbox" id="publication-card">
		<h3>Publication</h3>
		<ul>
			<li><a href="{{ 'external/publication/10WaysImproveEnglish' | relative_url }}">10 ways to improve your English</a></li>
			<li><a href="{{ 'external/publication/publishInBiotools' | relative_url }}">Publishing a tool in *bio.tools*</a></li>

		</ul>
	</div>
</div><br><center><a href="{{ '/' | relative_url }}">go back</a></center><br><center><a href="{{ '/cards' | relative_url }}">Overview of all HowTo cards</a></center>