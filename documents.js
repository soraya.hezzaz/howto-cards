---
---

{% assign the_cards = site.pages | where_exp:"card","card.searchable >= 1.0" %}

[ {% for card in the_cards %} {
            "id": "{{ card.url | slugify }}",
            "title": "{{ card.shortcut | xml_escape }}",
            "content": {{ card.content | markdownify | strip_html | jsonify }},
            "url": "{{ card.url | xml_escape | relative_url }}"
        } {% unless forloop.last %},{% endunless %} {% endfor %}
]
